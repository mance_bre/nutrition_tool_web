<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body class="new-recipe-page">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="wrapper">

        <div id="header" class="header">

            <ul class="nav" role="navigation">
                <li><a href="" class="analyze-recipe-button">Analyze Recipes</a></li>
                <li><a href="/app/new-recipe.php">New Recipe</a></li>
                <li><a href="">Export</a></li>
            </ul>

            <a href="javasctipt:void(0);" class="account-menu">
                <img src="../img/test-account-image.png" width="40px">
                <span class="account-data">
                    <span class="account-name">Jovan Stojanovic</span>
                    <span class="account-role">Manager at Agra Culture</span>
                </span>
                <span class="arrow-bottom"></span>
            </a>

            <p class="upgrade-message">A La Carte - <a href="">Upgrade to Full Course</a></p>

        </div>

        <div id="main" class="container">

            <div class="sidebar">

                <div class="brand">
                    <a href="/app/">
                        <img src="../img/test-logo.png" width="118px">
                    </a>
                    <h2>Agra Culture</h2>
                    <a href="http://www.agra-culture.com">www.agra-culture.com</a>
                </div>

                <ul class="sidebar-nav">
                    <li class="title">MENU</li>
                    <li><a href="" class="current">All Items →</a></li>
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU CATEGORIES</li>
                    <li><a href="">- Breakfast</a></li>
                    <li><a href="">- Lunch/Dinner</a></li>
                    <li><a href="">- Drinks</a></li>
                    <li><a href="">- Kids</a></li>
                    <li><a href="">- Dessert</a></li>
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU LABELING</li>
                    <li><a href="">- Paleo</a></li>
                    <li><a href="">- Vegetarian</a></li>
                    <li><a href="">- Vegan</a></li>
                    <li><a href="">- Heart Healthy</a></li>
                    <li><a href="">- Dairy Free</a></li>
                    <li><a href="">- Gluten Free</a></li>
                    <li><a href="">- High Protein</a></li>
                    <li><a href="">- Low Carb</a></li>
                    <li><a href="">- Low Sodium</a></li>
                    <li><a href="">- Low Calorie</a></li>
                    <li><a href="">- Low Fat</a></li>
                    <li><a href="">- Mediterranean</a></li>
                    <li><a href="">- Pescetarian</a></li>
                </ul>

            </div>
            <div class="content">
                <div class="recipe-actions">
                    <a href="edit" class="edit-recipe">Edit Recipe</a>
                </div>

                <ul class="breadcrumbs-holder">
                    <li><a href="/app/">All Dish »</a></li>
                    <li><a href="/app/">Lunch/Dinner »</a></li>
                    <li>Chicken Kebabs with Creamy Pesto</li>
                </ul>
                <div class="headline">
                    <h2>Chicken Kebabs with Creamy Pesto</h2>
                    <p class="headeline-details">in Lunch/Dinner <span class="paleo-badge">Paleo</span> <span class="certified-badge">Certified Nutrition</span></p>
                </div>

                <div class="recipe-details-holder">
                    <div class="details-left">
                        <div class="recipe-headline">
                            <h2>Yeild</h2>
                        </div>
                        <ul class="recipe-details">
                            <li>4 Servings</li>
                            <li>Serving size: 1 kebab and 1 tablespoon sauce</li>
                        </ul>

                        <div class="recipe-headline">
                            <h2>Ingredients</h2>
                        </div>
                        <ul class="recipe-details">
                            <li>2 teaspoons grated lemon rind</li>
                            <li>4 teaspoons fresh lemon juice, divided</li>
                            <li>2 teaspoons bottled minced garlic</li>
                            <li>2 teaspoons olive oil</li>
                            <li>1/2 teaspoon salt</li>
                            <li>1/4 teaspoon black pepper</li>
                            <li>8 (1-inch) pieces yellow bell pepper</li>
                            <li>8 cherry tomatoes</li>
                            <li>1 pound skinless, boneless chicken breasts, cut into 1-inch pieces</li>
                            <li>1 small red onion, cut into 8 wedges</li>
                            <li>Cooking spray</li>
                            <li>2 tablespoons plain low-fat yogurt</li>
                            <li>2 tablespoons reduced-fat sour cream</li>
                            <li>1 tablespoon commercial pesto</li>
                        </ul>

                        <div class="recipe-headline">
                            <h2>Preparation</h2>
                        </div>
                        <ul class="recipe-details cooking-steps">
                            <li>
                                <span class="cooking-number">1</span>
                                <span class="cooking-content">Preheat broiler.</span>
                            </li>
                            <li>
                                <span class="cooking-number">2</span>
                                <span class="cooking-content">Combine rind, 1 tablespoon juice, garlic, oil, salt, and pepper. Toss with bell pepper, tomatoes, chicken, and onion. Thread vegetables and chicken onto 4 (12-inch) skewers. Place skewers on a broiler pan coated with cooking spray. Broil 12 minutes or until chicken is done, turning occasionally.</span>
                            </li>
                            <li>
                                <span class="cooking-number">3</span>
                                <span class="cooking-content">Combine 1 teaspoon juice, yogurt, sour cream, and pesto. Serve sauce with kebabs.</span>
                            </li>
                        </ul>
                    </div>

                    <div class="details-right">
                        <div class="recipe-headline">
                            <h2>Recipe Images</h2>
                        </div>
                        <div class="recipe-gallery">
                            <div><img src="../../img/recipe-placeholder@2x.png"></div>
                            <div><img src="../../img/recipe-placeholder@2x.png"></div>
                            <div><img src="../../img/recipe-placeholder@2x.png"></div>
                            <div><img src="../../img/recipe-placeholder@2x.png"></div>
                        </div>

                        <div class="recipe-headline">
                            <h2>Nutrition Facts</h2>
                        </div>
                        <div class="nutrition-facts-holder">
                            <ul class="nutrition-facts-title">
                                <li>Serving Size: <span class="nutri-value">1 serving - 105.5g</span></li>
                                <li>Servings: <span class="nutri-value">8 -1/2 cup portions per recipe</span></li>
                            </ul>

                            <ul class="nutrition-facts-content">
                                <li class="daily-value">% Daily Value %</li>
                                <li>Calories  <span class="nutri-value">211</span></li>
                                <li>Fat <span class="nutri-value">7.3 g</span></li>
                                <li>Satfat <span class="nutri-value">2.1 g</span></li>
                                <li>Monofat <span class="nutri-value">3 g</span></li>
                                <li>Polyfat <span class="nutri-value">0.7 g</span></li>
                                <li>Protein <span class="nutri-value">27.9g</span></li>
                                <li>Carbohydrate <span class="nutri-value">7 g</span></li>
                                <li>Fiber <span class="nutri-value">1.2 g</span></li>
                                <li>Cholesterol <span class="nutri-value">70 mg</span></li>
                                <li>Iron <span class="nutri-value">1.4 mg</span></li>
                                <li>Sodium <span class="nutri-value">441 mg</span></li>
                                <li>Calcium <span class="nutri-value">48 mg</span></li>
                                <li>&nbsp;</li>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div id="footer" class="footer">

        </div>
    </div>
    <script src="../js/vendor/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="../js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="../js/vendor/slick.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function(){
            $('.recipe-gallery').slick({
                dots: true,
                infinite: true,
                speed: 500,
                fade: true,
                slide: 'div',
                cssEase: 'linear'
            });
        });
    </script>
</body>
</html>