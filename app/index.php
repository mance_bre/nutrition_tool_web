<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">

    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.2/underscore-min.js" type="text/javascript"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/0.9.2/backbone-min.js"></script>
    <script type="text/javascript" src="../js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="../js/vendor/slick.min.js"></script>
    <script type="text/javascript" src="../js/vendor/jquery.select.js"></script>
</head>
<body class="dashboard-page">

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="wrapper" class="wrapper">

        <div id="header" class="header">

            <ul class="nav" role="navigation">
                <li><a href="../app/#/new-recipe" class="analyze-recipe-button">Add Recipe</a></li>
                <li><a href="">Export</a></li>
            </ul>

            <div class="account-menu">
            </div>

            <p class="upgrade-message">A La Carte - <a href="">Upgrade to Full Course</a></p>

        </div>

        <div id="main" class="container">

            <div id="sidebar" class="sidebar">

                <div class="brand">
                </div>

                <ul class="sidebar-nav">
                    <li class="title">MENU</li>
                    <li><a href="" class="current">All Items →</a></li>
                    <li><a href="">- In Progress</a></li>
                    <li><a href="">- Verified</a></li>
                    <li><a href="">- Archive</a></li>
                </ul>

                <ul class="sidebar-nav groups-nav">
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU LABELING</li>
                    <li><a href="">- Paleo</a></li>
                    <li><a href="">- Vegetarian</a></li>
                    <li><a href="">- Vegan</a></li>
                    <li><a href="">- Heart Healthy</a></li>
                    <li><a href="">- Dairy Free</a></li>
                    <li><a href="">- Gluten Free</a></li>
                    <li><a href="">- High Protein</a></li>
                    <li><a href="">- Low Carb</a></li>
                    <li><a href="">- Low Sodium</a></li>
                    <li><a href="">- Low Calorie</a></li>
                    <li><a href="">- Low Fat</a></li>
                    <li><a href="">- Mediterranean</a></li>
                    <li><a href="">- Pescetarian</a></li>
                </ul>

            </div>

            <div class="content"></div>

        </div>

        <div id="footer" class="footer">

        </div>
    </div>

    <!-- TEMPLATES -->

    <script type="text/template" id="recipe-list-template">
        <div class="headline">
            <h2>All Items</h2>
            <form action="" class="search-form">
                <input name="search" type="search" placeholder="Search" class="search-field">

                <div class="search-results">
                    <ul>
                        <li class="nothing-found">
                            <p>Sorry, nothing found!</p>
                            <span>Accross your dishes.</span>
                        </li>
                        <li>
                            <p>Seared Angus Beef</p>
                            <span>in Lunch-Dinner</span>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="actions-holder">
            <ul class="action-dropdown-list">
                <li>
                    <a href="../app/" class="single-filter" id="select-all">Select All</a>
                </li>
                <li>
                    <a id="newest" href="#" class="type-filter double-filter <%= window.localStorage.getItem('filter1') === 'newest' ? 'current' : '' %>">Newest</a>
                    <a id="oldest" href="#" class="type-filter double-filter <%= window.localStorage.getItem('filter1') === 'oldest' ? 'current' : '' %> last">Oldest</a>
                </li>
                <li>
                    <a href="#" style="display:none;" class="type-action single-filter" id="edit">Edit</a>
                </li>
                <li>
                    <a href="#" style="display:none;" class="type-action single-filter" id="duplicate">Duplicate</a>
                </li>
                <li>
                    <a href="#" style="display:none;" class="type-action single-filter" id="move-to">Move to</a>
                    <div class="action-dropdown">
                        <a href="">Breakfast</a>
                        <a href="">Lunch-Dinner</a>
                        <a href="">Drinks</a>
                        <a href="">Kids</a>
                        <a href="">Dessert</a>
                    </div>
                </li>
                <li>
                    <a href="#" style="display:none;" class="type-action remove-button single-filter" id="remove">Remove</a>
                </li>
                <li>
                    <a href="#" style="display:none;" class="type-action single-filter last" id="archive">Archive</a>
                </li>
            </ul>

            <div class="sort">
                <span>Sort:</span>
                <ul class="action-dropdown-list">
                    <li>
                        <a href="javascript:void(0);" id="a-z" class="sort-by double-filter current">A-Z</a>
                        <a href="javascript:void(0);" id="z-a" class="sort-by double-filter">Z-A</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="results-holder">
            <ul class="results">
                <% var empty = true; %>
                <% _.each(recipes, function(recipe) { %>
                    <li class="results-item">
                        <a href="">
                        <div id="<%= recipe._id %>" class="result-image">
                            <%= recipe.verified ? '<span class="verified">Verified</span>' : "" %>
                            <span class="in-progress">In Progress</span>
                        </div>
                        </a>
                        <div class="result-details">
                            <h3><a href=""><%= recipe.name.substr(0, 24) %>... </a></h3>
                            <span class="recipe-category">in <%= recipe.category %></span>
                            <%
                            var time = new Date(parseInt(recipe._id.toString().slice(0,8), 16)*1000);
                            var parsedTime = unixToPrettyTime(time.getTime());
                            %>
                            <span class="recipe-time"><%= parsedTime %></span>
                        </div>
                        <div class="result-info">
                            <input id="checkbox_<%= recipe._id %>" type="checkbox" name="select" class="checkbox">
                            <h3><%= recipe.name.substr(0, 48) %> </h3>
                            <p>
                            <%= recipe.ingredients.substr(0, 60) %>...
                            </p>
                            <div class="category-info">
                                <span class="cate"><%= recipe.category %></span>
                                <%= recipe.nutrilabel ? '<span class="nutrilabel">' + recipe.nutrilabel + '</span>' : "" %>
                            </div>
                            <% if (recipe.nutrition_info) { %>
                                <!--each loop-->
                                <div class="nutrition-info">
                                    <span>CAL: 510</span>
                                    <span>FAT: 39g</span>
                                    <span>CARBS: 8g</span>
                                </div>
                            <% } %>
                            <a href="../app/#/recipe-details/<%= recipe._id %>" class="details-button"><%= recipe.tra %>View Details</a>
                        </div>
                    </li><% empty = false; %>
                <% }) %>
            </ul>
            <% if (empty) { %>
            <div class="no-results">
                <h3>Look like you not have any recipe?</h3>
                <p>We have multiple ways to easly add recipes into dashboard.</p>
                <a href="">Add Manually</a>, <a href="">Copy/Paste</a> or <a href="">Multiple File Uploding</a>
            </div>
            <% } %>
        </div>
        <% if (!empty) { %>
        <div class="pagination-holder">
            <a id="prev" ref="0" href="#">Prev</a>
            <a id="next" ref="2" href="#">Next</a>
        </div>
        <% } %>
    </script>

    <script type="text/template" id="new-recipe-template">
        <div class="headline">
            <h2><%= recipe ? 'Edit' : 'New' %> Recipe</h2>
        </div>

        <div class="form-holder">
            <form class="new-recipe-form">
                <% if(recipe) { %>
                    <input type="hidden" name="id" value="<%= recipe._id %>" />
                <% }; %>
                <a href="javascript:void(0);" class="double-filter current">Manually</a>
                <a href="javascript:void(0);" class="double-filter last">By Upload</a>

                <fieldset>
                    <label for="recipe-name">Name</label>
                    <input value="<%= recipe ? recipe.name : '' %>" id="recipe-name" type="text" name="name" placeholder="Eg. Chocolate Desert with Orange" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="number-servings">Number of Servings</label>
                    <input value="<%= recipe ? recipe.servings : '' %>" id="number-servings" type="text" name="servings" placeholder="Number of Servings" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="recipe">Recipe</label>
                    <textarea id="recipe" class="textarea-field recipe-field" name="ingredients" placeholder="Add Your Recipe"><%= recipe ? recipe.ingredients.replace("  ", "\n") : '' %></textarea>
                </fieldset>
                <fieldset>
                    <label for="directions">Directions <span class="useful-info">Every new paragraph is a new step.</span></label>
                    <textarea id="directions" class="textarea-field" name="directions" placeholder="How to Make the Dish…"><%= recipe ? recipe.directions : '' %></textarea>
                </fieldset>
                <fieldset>
                    <label for="category">Choose Category</label>
                    <select id="category" name="category" class="select-field">

                        <%
                        var category = recipe ? recipe.category : '';
                        %>
                        <option value="">Breakfast, Lunch-Dinner, Dessert…</option>
                        <option <%= category === 'Breakfast' ? 'selected' : '' %> value="Breakfast">Breakfast</option>
                        <option <%= category === 'Lunch-Dinner' ? 'selected' : '' %> value="Lunch-Dinner">Lunch-Dinner</option>
                        <option <%= category === 'Dessert' ? 'selected' : '' %> value="Dessert">Dessert</option>
                        <%
                        if (groups !== null) {
                            _.each(groups, function(group) { %>
                                <option <%= category === group.name ? 'selected' : '' %> value="<%= group.name %>"><%= group.name %></option>
                            <% })
                        }
                        %>
                    </select>
                </fieldset>
                <fieldset>
                    <div class="message"></div>
                    <label for="upload-images">Images</label>
                    <ul class="upload-images">
                        <% if (recipe) { %>
                            <% if (recipe.image0) { %>
                                <li>
                                    <input type="hidden" name="image0" value="<%= recipe.image0 %>" class="recipe_images">
                                    <img src="http://api.helloself.co/<%= recipe.image0 %>">
                                    <span id="image0" class="img_delete"></span>
                                </li>
                            <% } %>
                            <% if (recipe.image1) { %>
                                <li>
                                    <input type="hidden" name="image1" value="<%= recipe.image1 %>" class="recipe_images">
                                    <img src="http://api.helloself.co/<%= recipe.image1 %>">
                                    <span id="image1" class="img_delete"></span>
                                </li>
                            <% } %>
                            <% if (recipe.image2) { %>
                                <li>
                                    <input type="hidden" name="image2" value="<%= recipe.image2 %>" class="recipe_images">
                                    <img src="http://api.helloself.co/<%= recipe.image2 %>">
                                    <span id="image2" class="img_delete"></span>
                                </li>
                            <% } %>
                            <% if (recipe.image3) { %>
                                <li>
                                    <input type="hidden" name="image3" value="<%= recipe.image3 %>" class="recipe_images">
                                    <img src="http://api.helloself.co/<%= recipe.image3 %>">
                                    <span id="image3" class="img_delete"></span>
                                </li>
                            <% } %>
                        <% } %>
                        <li class="upload-item-holder">
                            <a href="javascript:void(0);" class="upload-image-button">
                                Upload
                                <input type="file" name="image" class="upload-field" multiple>
                            </a>
                            <div class="upload-progress"></div>
                        </li>
                    </ul>
                </fieldset>
                <fieldset>
                    <button name="submit" value="submit" type="submit" class="new-recipe-button"><%= recipe ? 'Update' : 'Create' %></button>
                </fieldset>
            </form>
            <div class="floating-section">
                <h4>Nutrition Analytics</h4>
                <a href="javascript:void(0);" class="double-filter current">On</a>
                <a href="javascript:void(0);" class="double-filter last">Off</a>
                <p>Nutrition Analytics are turned ON</p>
                <hr>
                <h4 class="recipe-name">Chocolate Desert with Orange</h4>
                <div class="cart-price">Total: $7</div>
                <p class="small-message">After recipe submission, you will redirect to our payment page. Your recipe will be saved to your account.</p>
            </div>
        </div>
    </script>

    <script type="text/template" id="recipe-details-template">
        <ul class="breadcrumbs-holder">
            <li><a href="/app/">All Dish »</a></li>
            <li><a href="/app/"><%= recipe.category %> »</a></li>
            <li><%= recipe.name %></li>
        </ul>
        <div class="recipe-actions">
            <a href="../app/#/edit-recipe/<%= recipe._id %>" class="edit-recipe">Edit Recipe</a>
        </div>
        <div class="headline">
            <h2><%= recipe.name %></h2>
            <p class="headeline-details">in <%= recipe.category %> <%= recipe.verified ? '<span class="paleo-badge">Paleo</span> <span class="certified-badge">Certified Nutrition</span></p>' : "" %>
        </div>

        <div class="recipe-details-holder">
            <div class="details-left">
                <div class="recipe-headline">
                    <h2>Yield</h2>
                </div>
                <ul class="recipe-details">
                    <li><%= recipe.servings %> Servings</li>
                    <!--li>Serving size: 1 kebab and 1 tablespoon sauce</li-->
                </ul>

                <div class="recipe-headline">
                    <h2>Ingredients</h2>
                </div>
                <ul class="recipe-details">
                <%
                    var ingredients = recipe.ingredients.split('\n');
                    for(var i = 0; i < ingredients.length; i++){ %>
                        <li> <%= ingredients[i] %> </li>
                    <% }
                %>
                </ul>

                <div class="recipe-headline">
                    <h2>Preparation</h2>
                </div>
                <ul class="recipe-details cooking-steps">

                <%
                    var directions = recipe.directions.split('\n');
                    var count = 0;
                    for(var i = 0; i < directions.length; i++){
                        if ( directions[i].trim() != '') {
                            count++ %>
                            <li>
                                <span class="cooking-number"><%= count %></span>
                                <span class="cooking-content"><%= directions[i] %> </span>
                            </li>
                    <% }}
                %>
                </ul>
            </div>

            <div class="details-right">
                <% if (recipe.image0 || recipe.image1 || recipe.image2 || recipe.image3) { %>
                    <div class="recipe-headline">
                        <h2>Recipe Images</h2>
                    </div>
                    <%= '<div class="recipe-gallery">' %>
                <% } %>
                        <% if (recipe.image0) { %>
                            <%= '<div><img src="http://api.helloself.co/' + recipe.image0 + '"></div>' %>
                        <% } %>
                        <% if (recipe.image1) { %>
                            <%= '<div><img src="http://api.helloself.co/' + recipe.image1 + '"></div>' %>
                        <% } %>
                        <% if (recipe.image2) { %>
                            <%= '<div><img src="http://api.helloself.co/' + recipe.image2 + '"></div>' %>
                        <% } %>
                        <% if (recipe.image3) { %>
                            <%= '<div><img src="http://api.helloself.co/' + recipe.image3 + '"></div>' %>
                        <% } %>
                <% if (recipe.image0 || recipe.image1 || recipe.image2 || recipe.image3) { %>
                    <%= '</div>' %>
                <% } %>

                <!--<div class="recipe-headline">
                    <h2>Nutrition Facts</h2>
                </div>-->
                <!--<div class="nutrition-facts-holder">
                    <ul class="nutrition-facts-title">
                        <li>Serving Size: <span class="nutri-value">1 serving - 105.5g</span></li>
                        <li>Servings: <span class="nutri-value">8 -1/2 cup portions per recipe</span></li>
                    </ul>

                    <ul class="nutrition-facts-content">
                        <li class="daily-value">% Daily Value %</li>
                        <li>Calories  <span class="nutri-value">211</span></li>
                        <li>Fat <span class="nutri-value">7.3 g</span></li>
                        <li>Satfat <span class="nutri-value">2.1 g</span></li>
                        <li>Monofat <span class="nutri-value">3 g</span></li>
                        <li>Polyfat <span class="nutri-value">0.7 g</span></li>
                        <li>Protein <span class="nutri-value">27.9g</span></li>
                        <li>Carbohydrate <span class="nutri-value">7 g</span></li>
                        <li>Fiber <span class="nutri-value">1.2 g</span></li>
                        <li>Cholesterol <span class="nutri-value">70 mg</span></li>
                        <li>Iron <span class="nutri-value">1.4 mg</span></li>
                        <li>Sodium <span class="nutri-value">441 mg</span></li>
                        <li>Calcium <span class="nutri-value">48 mg</span></li>
                        <li>&nbsp;</li>
                    </ul>

                </div>-->
            </div>
        </div>
    </script>

    <script type="text/template" id="groups-nav-template">
        <li class="title">MENU CATEGORIES <a href="/" class="edit-group">Edit</a></li>
        <% _.each(categories.get('defaultCategories'), function(category) { %>
        <li><a href="../app/#/searchbygroup/<%= category %>">- <%= category %></a></li>
        <% }) %>

        <% _.each(categories.get('categories'), function(category) { %>
        <li id="<%= category._id %>" ref="<%= category.name %>"><a href="../app/#/searchbygroup/<%= category.name %>">- <%= category.name %></a></li>
        <% }) %>
        <li>
        <a href="/" class="new-group-action">+ Add New</a>
        <div class="add-new-group" id="menuCategoryForm">
            <form action="/">
                <input type="text" name="new-group" class="new-group-field" placeholder="Add Group Name">
                <button class="new-group-button">Submit</button>
            </form>
        </div>

        </li>
    </script>

    <script type="text/template" id="profile-form-template">
        <div class="headline">
            <h2>Company Profile</h2>
        </div>

        <div class="form-holder">
            <form id="profile-form">
                <% if(profile && profile.get('_id')) { %>
                <input type="hidden" name="id" value="<%= profile.get('_id') %>" />
                <% }; %>

                <fieldset>
                    <label for="user-name">Company Name</label>
                    <input value="<%= profile ? profile.get('name') : '' %>" id="user-name" type="text" name="name" placeholder="Company Name" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="web-site">Web Site Url</label>
                    <input value="<%= profile ? profile.get('webSite') : '' %>" id="web-site" type="text" name="web-site" placeholder="eg. your-restaurant.com" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="country">Country</label>
                    <input value="<%= profile ? profile.get('country') : '' %>" id="country" type="text" name="country" placeholder="Country" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="state">State</label>
                    <input value="<%= profile ? profile.get('state') : '' %>" id="state" type="text" name="state" placeholder="State" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="city">City</label>
                    <input value="<%= profile ? profile.get('city') : '' %>" id="city" type="text" name="city" placeholder="City" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="address">Address</label>
                    <input value="<%= profile ? profile.get('address') : '' %>" id="address" type="text" name="address" placeholder="Address" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="zip-code">Zip Code</label>
                    <input value="<%= profile ? profile.get('zipCode') : '' %>" id="zip-code" type="text" name="zip-code" placeholder="Zip Code" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="phone-number">Phone Number</label>
                    <input value="<%= profile ? profile.get('phoneNumber') : '' %>" id="phone-number" type="text" name="phone-number" placeholder="eg. +(298) 223-234-22" class="input-field">
                </fieldset>
                <fieldset>
                    <button class="new-recipe-button">Submit</button>
                </fieldset>

                <div id="hidden-image"></div>
            </form>
            <div class="floating-right">
                <form action="">
                    <fieldset>
                        <label for="upload-images">Image</label>
                        <div class="upload-single-image logo-image">
                            <div class="image">
                                <% if(profile && profile.get('image')) { %>
                                <img src="http://api.helloself.co/<%= profile.get('image') %>" alt="" />
                                <% }; %>
                            </div>
                            <% if(!profile || !profile.get('image') || profile.get('image') == '') { %>
                            <a href="javascript:void(0);" class="upload-image-button">
                                Upload
                                <input type="file" name="image" class="upload-field">
                            </a>
                            <% }; %>
                            <a href="javascript:void(0);" class="change-image-button">
                                Upload
                                <input type="file" name="image" class="change-field">
                            </a>
                            <div class="upload-progress"></div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </script>

    <script type="text/template" id="company-logo-template">
        <a href="/app/">
            <img src="<%= profile.get('image') ? 'http://api.helloself.co/' + profile.get('image') : '../img/logo-placeholder.svg' %>" width="118px">
        </a>
        <h2><%= profile.get('name') ? profile.get('name') : '' %></h2>
        <a target="_blank" href="<%= profile.get('webSite') ? 'http://' + profile.get('webSite') : 'http://www.helloself.co' %>"><%= profile.get('webSite') ? profile.get('webSite') : 'www.helloself.co' %></a>
    </script>

    <script type="text/template" id="account-details-form-template">
        <div class="headline">
            <h2>Account Settings</h2>
        </div>

        <div class="form-holder">
            <form id="account-form">

                <% if(account && account.get('_id')) { %>
                <input type="hidden" name="id" value="<%= account.get('_id') %>" />
                <% }; %>

                <fieldset>
                    <label for="user-name">User Name</label>
                    <input value="<%= account ? account.get('user') : '' %>" id="user-name" type="text" name="user" placeholder="User Name" class="input-field" disabled>
                </fieldset>
                <fieldset>
                    <label for="first-last-name">First/Last Name</label>
                    <input value="<%= account ? account.get('full_name') : '' %>" id="first-last-name" type="text" name="full_name" placeholder="First Last Name" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="email-address">Email Address</label>
                    <input value="<%= account ? account.get('email') : '' %>" id="email-address" type="text" name="email" placeholder="Email Address" class="input-field" disabled>
                </fieldset>
                <fieldset>
                    <label for="company-role">Company Role</label>
                    <input value="<%= account ? account.get('company_role') : '' %>" id="company-role" type="text" name="company_role" placeholder="eg. Lead Chef" class="input-field">
                </fieldset>
                <fieldset>
                    <label for="phone-number">Phone Number</label>
                    <input value="<%= account ? account.get('phone') : '' %>" id="phone-number" type="text" name="phone" placeholder="eg. +(298) 223-234-22" class="input-field">
                </fieldset>
                <fieldset>
                    <button class="new-recipe-button">Submit</button>
                </fieldset>

                <div id="hidden-image"></div>
            </form>
            <div class="floating-right">
                <form action="">
                    <fieldset>
                        <label for="upload-images">Image</label>
                        <div class="upload-single-image user-image">
                            <div class="image">
                                <% if(account && account.get('_id')) { %>
                                <img src="http://api.helloself.co/<%= account.get('image') %>" alt="" />
                                <% }; %>
                            </div>
                            <% if(!account || !account.get('image') || account.get('image') == '') { %>
                            <a href="javascript:void(0);" class="upload-image-button">
                                Upload
                                <input type="file" name="" class="upload-field">
                            </a>
                            <% }; %>
                            <a href="javascript:void(0);" class="change-image-button">
                                Upload
                                <input type="file" name="image" class="change-field">
                            </a>
                            <div class="upload-progress"></div>
                        </div>
                    </fieldset>
                    <div class="upgrade-plan">
                        <p>PRICING PLAN</p>
                        <h2>A La Carte</h2>
                        <a href="/">Upgrade to Full Course</a>
                    </div>
                </form>
            </div>
        </div>
    </script>

    <script type="text/template" id="account-details-block-template">
        <a href id="accountOpen" class="account-open">
            <img src="<%= account.get('image') ? 'http://api.helloself.co/'+account.get('image') : '' %>">
                <span class="account-data">
                    <span class="account-name"><%= account.get('full_name') ? account.get('full_name') : account.get('user') %></span>
                    <span class="account-role"><%= account.get('company_role') ? account.get('company_role') : '' %></span>
                </span>
            <span class="arrow-bottom"></span>
        </a>

        <ul class="account-dropdown">
            <li><a href="../app/#/profile">Profile</a></li>
            <li><a href="../app/#/account">Account Settings</a></li>
            <li><a href="/">Billing</a></li>
            <li><a href="../app/#/logout" class="logout">Sign Out »</a></li>
        </ul>
    </script>

    <!-- END TEMPLATES -->

    <script>

        /* FUNCTIONS */

        var apiURL = "http://api.helloself.co:8080/";

        if (window.location.host === "localhost") {
            apiURL = "http://127.0.0.1:8080/";
        }

        $.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
            options.url = apiURL + options.url;
        });

        var access_token = window.localStorage.getItem('access_token');
        var token_expires = window.localStorage.getItem('token_expires');
        var unixNow = (new Date).getTime();
        if (!access_token || !token_expires || unixNow > token_expires) {
            window.location.replace("../account/#/signin");
        } else {
            $.ajaxSetup({
               headers: {
                 "accept": "application/json",
                 "access_token": access_token
               }
            });
        }

        $.fn.serializeObject = function() {
          var o = {};
          var a = this.serializeArray();
          $.each(a, function() {
              if (o[this.name] !== undefined) {
                  if (!o[this.name].push) {
                      o[this.name] = [o[this.name]];
                  }
                  o[this.name].push(this.value || '');
              } else {
                  o[this.name] = this.value || '';
              }
          });
          return o;
        };

        function getRecipeIdFromCheckbox(checkbox) {
            var checkboxId = $(checkbox).attr('id');
            if (typeof checkboxId === undefined) {
                console.log("checkbox id is undefined");
                return false;
            }

            var tempArray = checkboxId.split("_");
            return tempArray[1];
        }

        function unixToPrettyTime(unixTime) {
            var recipeTime = new Date(unixTime);
            var now = new Date();
            var timeValue;
            var words;

            var timeDiff = (now - recipeTime) / 1000;

            if (timeDiff > 3600 && timeDiff < (3600*30)) {
                timeValue = Math.round(timeDiff/3600);
                words = timeValue !== 1 ? " days ago" : " day ago";
                return timeValue + words;
            } else if (timeDiff > 60 && timeDiff < 3600) {
                timeValue = Math.round(timeDiff/60);
                words = timeValue !== 1 ? " minutes ago" : " minute ago";
                return timeValue + words;
            } else if (timeDiff < 60) {
                timeValue = Math.round(timeDiff);
                words = timeValue !== 1 ? " secunds ago" : " secund ago";
                return timeValue + words;
            } else {
                var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                var date = recipeTime.getDate() > 9 ? recipeTime.getDate() : "0" + recipeTime.getDate();
                var month = months[recipeTime.getMonth()];
                var year = recipeTime.getFullYear();
                return month + "/" + date + "/" + year;
            }
        }

        /* END FUNCTIONS */

        /* COLLECTIONS */

        var Recipes = Backbone.Collection.extend({
            url: '/recipe'
        });

        /* END COLLECTIONS */

        /* MODELS */

        var Recipe = Backbone.Model.extend({
            urlRoot: '/recipe'
        });

        var Upload = Backbone.Model.extend({
            urlRoot: '/upload'
        });

        var Duplicate = Backbone.Model.extend({
            urlRoot: '/duplicate'
        });

        var Archive = Backbone.Model.extend({
            urlRoot: '/archive'
        });

        var MenuCategories = Backbone.Model.extend({
            defaults: {
                defaultCategories: {
                    1 : 'Breakfast',
                    2 : 'Lunch-Dinner',
                    3 : 'Dessert'
                }
            },
            urlRoot: '/menu_category'
        });

        var CompanyProfile = Backbone.Model.extend({
           urlRoot: '/company/profile/'
        });

        var AccountDetails = Backbone.Model.extend({
            urlRoot: 'account/details'
        });

        /* END MODELS */

        /* VIEWS */

        var RecipeList = Backbone.View.extend({
            el: '.content',
            render: function(options) {
                var params = {};
                params.filter1 = window.localStorage.getItem('filter1') ? window.localStorage.getItem('filter1') : false;

                var page = window.localStorage.getItem('page') ? window.localStorage.getItem('page') : 1;
                var sort = window.localStorage.getItem('sort') ? window.localStorage.getItem('sort') : false;
                params.sort = sort;
                var that = this;
                var recipes = new Recipes();
                var unixNow = (new Date).getTime();
                params.page = page;

                if (typeof options !== 'undefined') {
                    params.group = options.group;
                    params.page = 1;
                }

                recipes.fetch({
                    data: params,
                    success: function(recipes) {
                        var template = _.template($("#recipe-list-template").html(), {recipes: recipes.models[0].get('recipes')});
                        that.$el.html(template);

                        var recipesObject = recipes.models[0].get('recipes');
                        var count = recipes.models[0].get('count');
                        var limit = recipes.models[0].get('limit');
                        var page = recipes.models[0].get('page');

                        $("#newest").removeClass("current");
                        $("#oldest").removeClass("current");
                        $(".sort-by").removeClass("current");
                        $(".double-filter").each(function() {
                            if ($(this).text() == sort) {
                                $(this).addClass("current");
                            }
                            if ($(this).text().toLowerCase().trim() == params.filter1) {
                                $(this).addClass("current");
                            }
                        });

                        if (count <= limit) {
                            $(".pagination-holder").hide();
                        }

                        recipesObject.forEach(function(recipe) {
                            if (recipe.image0 !== undefined || recipe.image1 !== undefined || recipe.image2 !== undefined || recipe.image3 !== undefined) {
                                var image = recipe.image0 !== undefined ? recipe.image0 : false;
                                if (!image) {
                                    image = recipe.image1 !== undefined ? recipe.image1 : false;
                                }
                                if (!image) {
                                    image = recipe.image2 !== undefined ? recipe.image2 : false;
                                }
                                if (!image) {
                                    image = recipe.image3 !== undefined ? recipe.image3 : false;
                                }
                                $("#" + recipe._id).css('background-image','url(http://api.helloself.co/' + image + ')');
                            }
                        });

                        if (count / limit < page) {
                            $("#next").addClass("last-pag");
                            $("#prev").removeClass("last-pag");
                        }
                        if (page == 1) {
                            $("#next").removeClass("last-pag");
                            $("#prev").addClass("last-pag");
                        }

                        $("#next").attr("ref", parseInt(page) + 1);
                        $("#prev").attr("ref", parseInt(page) - 1);

                        window.localStorage.setItem("page", page);
                    }
                });
            },
            events: {
                'click #next' : 'next',
                'click #prev' : 'prev',
                'click #select-all' : 'selectAll',
                'click .checkbox' : 'selectOne',
                'click #duplicate' : 'duplicate',
                'click #archive' : 'archive',
                'click #remove' : 'archive',
                'click #move-to' : 'moveToDropbox',
                'click #sort' : 'sortDropbox',
                'click .sort-by' : 'sortBy',
                'click .moveto' : 'moveTo',
                'click #newest' : 'newest',
                'click #oldest' : 'oldest',
                'click #edit' : 'edit'
            },
            edit: function() {
                var count = 0;
                var id
                $('.checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        id = getRecipeIdFromCheckbox(this);
                        count++;
                    }
                });

                if (count === 1) {
                    router.navigate('/#/edit-recipe/' + id, {trigger:true});
                }

                return false;
            },
            newest: function(ev) {
                window.localStorage.setItem('filter1', 'newest');
                window.localStorage.setItem('sort', false);

                recipeList.render();
            },
            oldest: function(ev) {
                window.localStorage.setItem('filter1', 'oldest');
                window.localStorage.setItem('sort', false);

                recipeList.render();
            },
            moveTo: function(ev) {
                $(".action-dropdown").hide();
                var category = $(ev.currentTarget).text();
                var recipe = new Recipe();
                var that = this;

                $('.checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        var id = getRecipeIdFromCheckbox(this);

                        recipe.save({id: id, category: category}, {
                            success: function(model, response) {
                                console.log(response.message);
                            },
                            error: function(model, response) {
                                console.log(response.responseText);
                            }
                        });


                        var params = {};

                        params.filter1 = window.localStorage.getItem('filter1') ? window.localStorage.getItem('filter1') : false;

                        var page = window.localStorage.getItem('page') ? window.localStorage.getItem('page') : 1;
                        var sort = window.localStorage.getItem('sort') ? window.localStorage.getItem('sort') : "A-Z";
                        if (sort.length > 0) {
                            window.localStorage.setItem('sort', sort);
                        }
                        var recipes = new Recipes();
                        var unixNow = (new Date).getTime();
                        params.page = page;
                        params.sort = sort;
                        recipes.fetch({
                            data: params,
                            success: function(recipes) {
                                var template = _.template($("#recipe-list-template").html(), {recipes: recipes.models[0].get('recipes')});
                                that.$el.html(template);

                                $("#newest").removeClass("current");
                                $("#oldest").removeClass("current");
                                $(".sort-by").removeClass("current");
                                $(".double-filter").each(function() {
                                    if ($(this).text() == sort) {
                                        $(this).addClass("current");
                                    }
                                    if ($(this).text().toLowerCase().trim() == params.filter1) {
                                        $(this).addClass("current");
                                    }
                                });

                                var recipesObject = recipes.models[0].get('recipes');
                                var count = recipes.models[0].get('count');
                                var limit = recipes.models[0].get('limit');
                                var page = recipes.models[0].get('page');

                                if (count <= limit) {
                                    $(".pagination-holder").hide();
                                }

                                recipesObject.forEach(function(recipe) {
                                    if (recipe.image0 !== undefined || recipe.image1 !== undefined || recipe.image2 !== undefined || recipe.image3 !== undefined) {
                                        var image = recipe.image0 !== undefined ? recipe.image0 : false;
                                        if (!image) {
                                            image = recipe.image1 !== undefined ? recipe.image1 : false;
                                        }
                                        if (!image) {
                                            image = recipe.image2 !== undefined ? recipe.image2 : false;
                                        }
                                        if (!image) {
                                            image = recipe.image3 !== undefined ? recipe.image3 : false;
                                        }
                                        $("#" + recipe._id).css('background-image','url(http://api.helloself.co/' + image + ')');
                                    }
                                });

                                if (count / limit < page) {
                                    $("#next").addClass("last-pag");
                                    $("#prev").removeClass("last-pag");
                                }
                                if (page == 1) {
                                    $("#next").removeClass("last-pag");
                                    $("#prev").addClass("last-pag");
                                }

                                $("#next").attr("ref", parseInt(page) + 1);
                                $("#prev").attr("ref", parseInt(page) - 1);

                                window.localStorage.setItem("page", page);
                            }
                        });
                    }
                });

                return false;
            },
            sortBy: function(ev) {
                var params = {};

                window.localStorage.setItem('filter1', false);
                params.filter1 = false;

                var page = window.localStorage.getItem('page') ? window.localStorage.getItem('page') : 1;
                var sort = $(ev.currentTarget).text();
                if (sort.length > 0) {
                    window.localStorage.setItem('sort', sort);
                }

                var that = this;
                var recipes = new Recipes();
                var unixNow = (new Date).getTime();
                params.page = page;
                params.sort = sort;
                recipes.fetch({
                    data: params,
                    success: function(recipes) {
                        var template = _.template($("#recipe-list-template").html(), {recipes: recipes.models[0].get('recipes')});
                        that.$el.html(template);

                        $("#newest").removeClass("current");
                        $("#oldest").removeClass("current");
                        $(".sort-by").removeClass("current");
                        $(".double-filter").each(function() {
                            if ($(this).text() == sort) {
                                $(this).addClass("current");
                            }
                            if ($(this).text().toLowerCase().trim() == params.filter1) {
                                $(this).addClass("current");
                            }
                        });

                        var recipesObject = recipes.models[0].get('recipes');
                        var count = recipes.models[0].get('count');
                        var limit = recipes.models[0].get('limit');
                        var page = recipes.models[0].get('page');

                        if (count <= limit) {
                            $(".pagination-holder").hide();
                        }

                        recipesObject.forEach(function(recipe) {
                            if (recipe.image0 !== undefined || recipe.image1 !== undefined || recipe.image2 !== undefined || recipe.image3 !== undefined) {
                                var image = recipe.image0 !== undefined ? recipe.image0 : false;
                                if (!image) {
                                    image = recipe.image1 !== undefined ? recipe.image1 : false;
                                }
                                if (!image) {
                                    image = recipe.image2 !== undefined ? recipe.image2 : false;
                                }
                                if (!image) {
                                    image = recipe.image3 !== undefined ? recipe.image3 : false;
                                }
                                $("#" + recipe._id).css('background-image','url(http://api.helloself.co/' + image + ')');
                            }
                        });

                        if (count / limit < page) {
                            $("#next").addClass("last-pag");
                            $("#prev").removeClass("last-pag");
                        }
                        if (page == 1) {
                            $("#next").removeClass("last-pag");
                            $("#prev").addClass("last-pag");
                        }

                        $("#next").attr("ref", parseInt(page) + 1);
                        $("#prev").attr("ref", parseInt(page) - 1);

                        window.localStorage.setItem("page", page);
                    }
                });

                return false;
            },
            sortDropbox: function(ev) {
                $(ev.currentTarget).next(".action-dropdown").toggle();
                return false;
            },
            moveToDropbox: function(ev) {
                var categories = new MenuCategories();
                var dropbox = $(ev.currentTarget).next(".action-dropdown");

                categories.fetch({
                    success: function(model, response) {
                        var defaultCategories = categories.get('defaultCategories');
                        var groups = categories.get('categories');
                        var html = '';

                        for (var i in defaultCategories) {
                            html += '<a class="moveto" href>' + defaultCategories[i] + '</a>';
                        }
                        groups.forEach(function(category) {
                            html += '<a class="moveto" href>' + category.name + '</a>';
                        });
                        dropbox.html(html);
                        dropbox.toggle();
                    },
                    error: function(model, response) {
                        console.log(response);
                    }
                });
                return false;
            },
            next: function(ev) {
                if ($(ev.currentTarget).hasClass("last-pag")) {
                    return false;
                }

                var params = {};
                params.filter1 = window.localStorage.getItem('filter1') ? window.localStorage.getItem('filter1') : false;

                var page = $(ev.currentTarget).attr("ref");
                var sort = window.localStorage.getItem('sort') ? window.localStorage.getItem('sort') : "A-Z";
                $("#sortText").text(sort);
                var that = this;
                var recipes = new Recipes();
                var unixNow = (new Date).getTime();
                params.page = page;
                params.sort = sort;
                recipes.fetch({
                    data: params,
                    success: function(recipes) {
                        var template = _.template($("#recipe-list-template").html(), {recipes: recipes.models[0].get('recipes')});
                        that.$el.html(template);

                        $("#newest").removeClass("current");
                        $("#oldest").removeClass("current");
                        $(".sort-by").removeClass("current");
                        $(".double-filter").each(function() {
                            if ($(this).text() == sort) {
                                $(this).addClass("current");
                            }
                            if ($(this).text().toLowerCase().trim() == params.filter1) {
                                $(this).addClass("current");
                            }
                        });

                        var recipesObject = recipes.models[0].get('recipes');
                        var count = recipes.models[0].get('count');
                        var limit = recipes.models[0].get('limit');

                        if (count <= limit) {
                            $(".pagination-holder").hide();
                        }

                        recipesObject.forEach(function(recipe) {
                            if (recipe.image0 !== undefined || recipe.image1 !== undefined || recipe.image2 !== undefined || recipe.image3 !== undefined) {
                                var image = recipe.image0 !== undefined ? recipe.image0 : false;
                                if (!image) {
                                    image = recipe.image1 !== undefined ? recipe.image1 : false;
                                }
                                if (!image) {
                                    image = recipe.image2 !== undefined ? recipe.image2 : false;
                                }
                                if (!image) {
                                    image = recipe.image3 !== undefined ? recipe.image3 : false;
                                }
                                $("#" + recipe._id).css('background-image','url(http://api.helloself.co/' + image + ')');
                            }
                        });

                        if (count / limit < page) {
                            $("#next").addClass("last-pag");
                            $("#prev").removeClass("last-pag");
                        }
                        if (page == 1) {
                            $("#next").removeClass("last-pag");
                            $("#prev").addClass("last-pag");
                        }

                        $("#next").attr("ref", parseInt(page) + 1);
                        $("#prev").attr("ref", parseInt(page) - 1);

                        window.localStorage.setItem("page", page);
                    }
                });
            },
            prev: function(ev) {
                if ($(ev.currentTarget).hasClass("last-pag")) {
                    return false;
                }

                var params = {};
                params.filter1 = window.localStorage.getItem('filter1') ? window.localStorage.getItem('filter1') : false;

                var page = $(ev.currentTarget).attr("ref");
                var sort = window.localStorage.getItem('sort') ? window.localStorage.getItem('sort') : "A-Z";

                var that = this;
                var recipes = new Recipes();
                var unixNow = (new Date).getTime();
                params.page = page;
                params.sort = sort;
                recipes.fetch({
                    data: params,
                    success: function(recipes) {
                        var template = _.template($("#recipe-list-template").html(), {recipes: recipes.models[0].get('recipes')});
                        that.$el.html(template);

                        $("#newest").removeClass("current");
                        $("#oldest").removeClass("current");
                        $(".sort-by").removeClass("current");
                        $(".double-filter").each(function() {
                            if ($(this).text() == sort) {
                                $(this).addClass("current");
                            }
                            if ($(this).text().toLowerCase().trim() == params.filter1) {
                                $(this).addClass("current");
                            }
                        });

                        var recipesObject = recipes.models[0].get('recipes');
                        var count = recipes.models[0].get('count');
                        var limit = recipes.models[0].get('limit');

                        if (count <= limit) {
                            $(".pagination-holder").hide();
                        }

                        recipesObject.forEach(function(recipe) {
                            if (recipe.image0 !== undefined || recipe.image1 !== undefined || recipe.image2 !== undefined || recipe.image3 !== undefined) {
                                var image = recipe.image0 !== undefined ? recipe.image0 : false;
                                if (!image) {
                                    image = recipe.image1 !== undefined ? recipe.image1 : false;
                                }
                                if (!image) {
                                    image = recipe.image2 !== undefined ? recipe.image2 : false;
                                }
                                if (!image) {
                                    image = recipe.image3 !== undefined ? recipe.image3 : false;
                                }
                                $("#" + recipe._id).css('background-image','url(http://api.helloself.co/' + image + ')');
                            }
                        });

                        if (count / limit < page) {
                            $("#next").addClass("last-pag");
                            $("#prev").removeClass("last-pag");
                        }
                        if (page == 1) {
                            $("#next").removeClass("last-pag");
                            $("#prev").addClass("last-pag");
                        }

                        $("#prev").attr("ref", parseInt(page) - 1);
                        $("#next").attr("ref", parseInt(page) + 1);

                        window.localStorage.setItem("page", page);
                    }
                });
            },
            selectAll: function(ev) {
                if ($(ev.currentTarget).hasClass('current')) {
                    $(ev.currentTarget).removeClass('current');
                    $(ev.currentTarget).html('Select All');

                    $(".type-action").hide();
                    $(".type-filter").show();

                    $('.checkbox').each(function() {
                        $(this).removeAttr('checked');
                        $(this).parent().removeClass("checked");
                    });
                } else {
                    $(ev.currentTarget).addClass('current');
                    $(ev.currentTarget).html('Deselect All');

                    $(".type-action").show();
                    $(".type-filter").hide();

                    $('.checkbox').each(function() {
                        $(this).attr('checked', 'checked');
                        $(this).parent().addClass("checked");
                    });
                }

                var checked = 0;
                var recipeIds = [];

                $('.checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        checked++;
                        recipeIds.push(getRecipeIdFromCheckbox(this));
                    }
                });

                return false;
            },
            selectOne: function(ev) {
                var checked = 0;
                var recipeIds = [];

                $('.checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        checked++;
                        recipeIds.push(getRecipeIdFromCheckbox(this));
                        $(this).parent().addClass("checked");
                    } else {
                        $(this).parent().removeClass("checked");
                    }
                });

                if (checked === 0) {
                    $(".type-action").hide();
                    $(".type-filter").show();
                } else if (checked > 0) {
                    $(".type-action").show();
                    $(".type-filter").hide();
                }
            },
            duplicate: function(ev) {
                var checked = 0;
                var recipeIds = [];

                $('.checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        checked++;
                        recipeIds.push(getRecipeIdFromCheckbox(this));
                    }
                });

                var duplicate = new Duplicate();
                duplicate.save({recipeIds: recipeIds}, {
                    dataType:"text",
                    success: function(model, response) {
                        var params = {};
                        params.filter1 = window.localStorage.getItem('filter1') ? window.localStorage.getItem('filter1') : false;
                        var sort = window.localStorage.getItem('sort') ? window.localStorage.getItem('sort') : false;
                        params.sort = sort;

                        var page = window.localStorage.getItem('page');
                        var that = this;
                        var recipes = new Recipes();
                        var unixNow = (new Date).getTime();
                        params.page = page;

                        recipes.fetch({
                            data: params,
                            success: function(recipes) {
                                var template = _.template($("#recipe-list-template").html(), {recipes: recipes.models[0].get('recipes')});
                                $(".content").html(template);

                                $("#newest").removeClass("current");
                                $("#oldest").removeClass("current");
                                $(".sort-by").removeClass("current");
                                $(".double-filter").each(function() {
                                    if ($(this).text() == sort) {
                                        $(this).addClass("current");
                                    }
                                    if ($(this).text().toLowerCase().trim() == params.filter1) {
                                        $(this).addClass("current");
                                    }
                                });

                                var recipesObject = recipes.models[0].get('recipes');
                                var count = recipes.models[0].get('count');
                                var limit = recipes.models[0].get('limit');

                                if (count <= limit) {
                                    $(".pagination-holder").hide();
                                }

                                recipesObject.forEach(function(recipe) {
                                    if (recipe.image0 !== undefined || recipe.image1 !== undefined || recipe.image2 !== undefined || recipe.image3 !== undefined) {
                                        var image = recipe.image0 !== undefined ? recipe.image0 : false;
                                        if (!image) {
                                            image = recipe.image1 !== undefined ? recipe.image1 : false;
                                        }
                                        if (!image) {
                                            image = recipe.image2 !== undefined ? recipe.image2 : false;
                                        }
                                        if (!image) {
                                            image = recipe.image3 !== undefined ? recipe.image3 : false;
                                        }
                                        $("#" + recipe._id).css('background-image','url(http://api.helloself.co/' + image + ')');
                                    }
                                });

                                if (count / limit < page) {
                                    $("#next").addClass("last-pag");
                                    $("#prev").removeClass("last-pag");
                                }
                                if (page == 1) {
                                    $("#next").removeClass("last-pag");
                                    $("#prev").addClass("last-pag");
                                }

                                $("#next").attr("ref", parseInt(page) + 1);
                                $("#prev").attr("ref", parseInt(page) - 1);

                                window.localStorage.setItem("page", page);
                            },
                            error: function(model, response) {
                                console.log('error', response.responseText);
                            }
                        });

                    },
                    error: function(model, response) {
                        console.log('error', response.responseText);
                    }
                });
            },
            archive: function(ev) {
                var checked = 0;
                var recipeIds = [];

                $('.checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        checked++;
                        recipeIds.push(getRecipeIdFromCheckbox(this));
                    }
                });

                var archive = new Archive();
                archive.save({recipeIds: recipeIds}, {
                    dataType:"text",
                    success: function(model, response) {
                        for (var i in recipeIds) {
                            $('#' + recipeIds[i]).parent().parent().remove();
                        }
                    },
                    error: function(model, response) {
                        console.log('error', model, response);
                    }
                });

                return false;
            }

        });

        var EditRecipe = Backbone.View.extend({
            el: '.content',
            render: function(option) {
                var that = this;
                if (option.id) {
                    var recipe = new Recipe({id: option.id});
                    recipe.fetch({
                        success: function(result) {
                            var template = _.template($("#new-recipe-template").html(), {recipe: result.get('recipe'), groups: recipe.get('categories')});
                            that.$el.html(template);
                            $('.select-field').selectOverlap();

                            if ($('.img_delete').length == 4) {
                                $(".upload-item-holder").hide();
                            } else if($('.img_delete').length < 4) {
                                $(".upload-item-holder").show();
                            }
                        }
                    });
                } else {
                    var menuCategories = new MenuCategories();
                    var that = this;
                    menuCategories.fetch({
                        success: function(result) {
                            var allGroups = [];
                            var customGroups = result.get('categories');
                            var defaultGroups = result.get('defaultCategories');

                            customGroups.forEach(function(group) {
                                allGroups.push({name: group.name});
                            });

                            var template = _.template($("#new-recipe-template").html(), {recipe: null, groups: allGroups});
                            that.$el.html(template);
                            $('.select-field').selectOverlap();
                        }
                    })
                }

            },
            events: {
                'submit .new-recipe-form' : 'saveRecipe',
                'change .upload-field' : 'uploadFile',
                'click .img_delete' : 'removeImage'
            },
            saveRecipe: function(ev) {
                var recipeDetails = $(ev.currentTarget).serializeObject();
                var recipe = new Recipe();
                recipe.save(recipeDetails, {
                    success: function(model, response) {
                        alert(response.message);
                        router.navigate('', {trigger:true});
                    },
                    error: function(model, response) {
                        $(".message").html(response.responseText);
                    }
                });
                // console.log(recipeDetails);
                return false;
            },
            uploadFile: function(ev) {
                var allowedExtension = ["jpg","jpeg","png"];
                var allowedFileSize = 5000000;
                var $fileUpload = $(ev.currentTarget);
                if ((parseInt($fileUpload.get(0).files.length) + $('.img_delete').length) > 4){
                    $(".message").html("You can only upload a maximum of 4 files");
                    return false;
                }

                for (var s in $fileUpload.get(0).files) {
                    if ($fileUpload.get(0).files[s].size !== undefined && $fileUpload.get(0).files[s].type !== undefined) {
                        if ($fileUpload.get(0).files[s].size > allowedFileSize) {
                            $(".message").html("Maximum image size is 5 mb");
                            return false;
                        }

                        var extensionPass = true;
                        for (var o in allowedExtension) {
                            var fileType = $fileUpload.get(0).files[s].type.toLowerCase();
                            if (fileType.indexOf(allowedExtension[o]) > -1) {
                                extensionPass = false;
                            }
                        }

                        if (extensionPass) {
                            $(".message").html("Allowed image formats are jpg, jpeg and png");
                            return false;
                        }
                    }
                }
                var pictureInput = $(ev.currentTarget)[0];
                var myFormData = new FormData();
                for (var i in pictureInput.files) {
                    if (!isNaN(i)) {
                        myFormData.append('image'+i, pictureInput.files[i]);
                    }
                }

                function updateProgress(evt) {
                    // console.log('updateProgress');
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total * 100 + '%');
                        // console.log(percentComplete);
                        if (evt.loaded !== evt.total) {
                            $('.upload-progress').width(percentComplete);
                        } else {
                            $('.upload-progress').width(0);
                        }

                    } else {
                        // Unable to compute progress information since the total size is unknown
                        console.log('unable to complete');
                    }
                }

                $.ajax({
                    url: 'upload_image',
                    type: 'POST',
                    data: myFormData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    xhr: function() {  // custom xhr
                        myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) { // check if upload property exists
                                myXhr.upload.addEventListener('progress',updateProgress, false); // for handling the progress of the upload
                            }
                            return myXhr;
                    },
                    success: function(result) {
                        $.each( result, function( i, val ) {
                            var imageName;
                            // if ($('.img_delete').length >0) {
                                var imageIds = ['image0', 'image1', 'image2', 'image3'];
                                var usedImageIds = [];
                                $('.img_delete').each(function() {
                                    usedImageIds.push(this.id);
                                });
                                var spareImageIds = $(imageIds).not(usedImageIds).get();
                                imageName = spareImageIds[0];
                            // } else {
                            //     imageName = i;
                            // }
                            var imgInput = '<input type="hidden" name="' + imageName + '" value="' + val.path + '" class="recipe_images">';
                            var image = '<li><img src="http://api.helloself.co/' + val.path + '"><span id="' + imageName + '" class="img_delete"></span></li>';
                            $(".upload-item-holder").before(image);
                            $("#category").after(imgInput);

                            if ($('.img_delete').length == 4) {
                                $(".upload-item-holder").hide();
                            } else if($('.img_delete').length < 4) {
                                $(".upload-item-holder").show();
                            }
                        });
                    },
                    error: function() {
                        $(".message").html('failure');
                    }
                });
            },
            removeImage: function(ev) {
                $(ev.currentTarget).parent("li").remove();
                $("[name='" + ev.currentTarget.id + "']").remove();

                if ($('.img_delete').length == 4) {
                    $(".upload-item-holder").hide();
                } else if($('.img_delete').length < 4) {
                    $(".upload-item-holder").show();
                }
            }
        });

        var RecipeDetails = Backbone.View.extend({
            el: '.content',
            render: function(option) {
                var that = this;
                if (option.id) {
                    var recipe = new Recipe({id: option.id});
                    recipe.fetch({
                        success: function(result) {
                            var template = _.template($("#recipe-details-template").html(), {recipe: result.get('recipe'), groups: recipe.get('categories')});
                            that.$el.html(template);
                            $('.recipe-gallery').slick({
                                dots: true,
                                infinite: true,
                                speed: 500,
                                fade: true,
                                slide: 'div',
                                cssEase: 'linear'
                            });
                        }
                    });
                }
            }
        });

    	var Logout = Backbone.View.extend({
    	    render: function() {
                window.localStorage.removeItem("user_id");
                window.localStorage.removeItem("user_email");
                window.localStorage.removeItem("user_role");
                window.localStorage.removeItem("access_token");
                window.localStorage.removeItem("token_expires");

                window.location.replace("../account/#/signin");
    	    }
    	});

        var ManyCategoriesView = Backbone.View.extend({
            el: '.groups-nav',
            render: function() {
                var categories = new MenuCategories();
                var that = this;

                categories.fetch({
                    success: function(categories) {
                        var template = _.template($("#groups-nav-template").html(), {categories: categories});
                        that.$el.html(template);
                    },
                    error: function(model, response) {
                        console.log(response);
                    }
                });
            },
            events: {
                'click .new-group-action' : 'newCategory',
                'submit #menuCategoryForm form' : 'addCategory',
                'click .edit-group' : 'editCategories',
                'keyup .category-input' : 'save',
                'click .category-delete' : 'delete'
            },
            newCategory: function(ev) {
                $("#menuCategoryForm").toggle();

                return false;
            },
            addCategory: function(ev) {
                var categoryName = $(ev.currentTarget).serializeObject();
                var categories = new MenuCategories();
                var that = this;

                categories.save(categoryName, {
                    success: function(model, response) {
                        that.render();
                    },
                    error: function(model, response) {
                        console.log(model, response);
                    }
                });

                return false;
            },
            editCategories: function() {
                if ($(".groups-nav li").hasClass("editing")) {
                    this.render();
                } else {
                    $(".groups-nav li").addClass("editing");
                    $(".groups-nav li").each(function() {
                        if (typeof this.id !== "undefined" && this.id.trim() !== "") {
                            var html = '<form class="category-edit">';
                            html += '<input type="text" name="category_name" value="' + $(this).attr("ref") + '" class="category-input" >';
                            html += '<span title="Delete This Category" class="category-delete">x</span>'
                            html += '<input type="hidden" name="id" value="' + this.id + '">'
                            html += '</form>';
                            $(this).html(html);
                        }
                    })
                }

                return false;
            },
            save: function(ev) {
                var editedCat = $(ev.currentTarget).parent("form").serializeObject();
                var categories = new MenuCategories();
                var that = this;

                categories.save(editedCat);

                return false;
            },
            delete: function(ev) {
                var id =$(ev.currentTarget).next().val();
                var categories = new MenuCategories({id:id});
                categories.destroy({
                    success: function(model, response) {
                        var categoryForm = $(ev.currentTarget).parent("form");
                        $(categoryForm).remove();
                    },
                    error: function(model, response) {
                        console.log(response.responseText);
                    }
                });
            }
        });

        var CompanyProfileForm = Backbone.View.extend({
            el: '.content',
            render: function(option) {
                var that = this;
                if (option.id) {
                    var profile = new CompanyProfile();
                    profile.fetch({
                        success: function(profile) {
                            var template = _.template($("#profile-form-template").html(), {profile: profile});
                            that.$el.html(template);
                        }
                    });
                } else {
                    var template = _.template($("#profile-form-template").html(), {profile: null});
                    this.$el.html(template);
                }
            },
            uploadFile: function(ev) {
                var allowedExtension = ["jpg","jpeg","png"];
                var allowedFileSize = 5000000;
                var $fileUpload = $(ev.currentTarget);

                for (var s in $fileUpload.get(0).files) {
                    if ($fileUpload.get(0).files[s].size !== undefined && $fileUpload.get(0).files[s].type !== undefined) {
                        if ($fileUpload.get(0).files[s].size > allowedFileSize) {
                            alert("Maximum image size is 5 mb");
                            return false;
                        }

                        var extensionPass = true;
                        for (var o in allowedExtension) {
                            var fileType = $fileUpload.get(0).files[s].type.toLowerCase();
                            if (fileType.indexOf(allowedExtension[o]) > -1) {
                                extensionPass = false;
                            }
                        }

                        if (extensionPass) {
                            alert("Allowed image formats are jpg, jpeg and png");
                            return false;
                        }
                    }
                }
                var pictureInput = $(ev.currentTarget)[0];
                var myFormData = new FormData();
                for (var i in pictureInput.files) {
                    if (!isNaN(i)) {
                        myFormData.append('image'+i, pictureInput.files[i]);
                    }
                }

                function updateProgress(evt) {
                    // console.log('updateProgress');
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total * 100 + '%');
                        // console.log(percentComplete);
                        if (evt.loaded !== evt.total) {
                            $('.upload-progress').width(percentComplete);
                        } else {
                            $('.upload-progress').width(0);
                        }

                    } else {
                        // Unable to compute progress information since the total size is unknown
                        console.log('unable to complete');
                    }
                }

                $.ajax({
                    url: 'upload_image',
                    type: 'POST',
                    data: myFormData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    xhr: function() {  // custom xhr
                        myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // check if upload property exists
                            myXhr.upload.addEventListener('progress',updateProgress, false); // for handling the progress of the upload
                        }
                        return myXhr;
                    },
                    success: function(result) { console.log(result)
                        var imgInput = '<input type="hidden" name="image0" value="' + result.image0.path + '" class="recipe_images">';
                        var image = '<img src="http://api.helloself.co/' + result.image0.path + '">';
                        $("#hidden-image").html(imgInput);
                        $(".image").html(image);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            },
            events: {
                'submit #profile-form': 'save',
                'change .upload-field' : 'uploadFile',
                'change .change-field' : 'uploadFile'
            },
            save: function(ev) {
                var form = $(ev.currentTarget).serializeObject();
                var profile = new CompanyProfile();

                profile.save(form, {
                    success: function(model, response) {
                        alert(response.message);
                        comapnyProfileForm.render({id: 123});
                        categoriesManu.render();
                        companyDetails.render();
                        accountBlock.render();
                    },
                    error: function(model, response) {
                        console.log(response.responseText);
                    }
                });

                return false;
            }

        });

        var CompanyDetailsView = Backbone.View.extend({
            el: '.brand',
            render: function(option) {
                var profile = new CompanyProfile();
                var that = this;
                profile.fetch({
                    success: function (profile) {
                        var template = _.template($("#company-logo-template").html(), {profile: profile});
                        that.$el.html(template);
                    }
                });

            }
        });

        var AccountDetailsForm = Backbone.View.extend({
            el: '.content',
            render: function(option) {
                var that = this;
                if (option.id) {
                    var account = new AccountDetails();
                    account.fetch({
                        success: function(account) {
                            var template = _.template($("#account-details-form-template").html(), {account: account});
                            that.$el.html(template);
                        }
                    });
                } else {
                    var template = _.template($("#account-details-form-template").html(), {profile: null});
                    this.$el.html(template);
                }
            },
            events: {
                'submit #account-form' : 'save',
                'change .upload-field' : 'uploadFile',
                'change .change-field' : 'uploadFile'
            },
            save: function(ev) {
                var form = $(ev.currentTarget).serializeObject();
                var account = new AccountDetails();

                account.save(form, {
                    success: function(model, response) {
                        alert(response.message);
                        accountForm.render({id: 123});
                        categoriesManu.render();
                        companyDetails.render();
                        accountBlock.render();
                    },
                    error: function(model, response) {
                        console.log(response.responseText);
                    }
                });

                return false;
            },
            uploadFile: function(ev) {
                var allowedExtension = ["jpg","jpeg","png"];
                var allowedFileSize = 5000000;
                var $fileUpload = $(ev.currentTarget);

                for (var s in $fileUpload.get(0).files) {
                    if ($fileUpload.get(0).files[s].size !== undefined && $fileUpload.get(0).files[s].type !== undefined) {
                        if ($fileUpload.get(0).files[s].size > allowedFileSize) {
                            alert("Maximum image size is 5 mb");
                            return false;
                        }

                        var extensionPass = true;
                        for (var o in allowedExtension) {
                            var fileType = $fileUpload.get(0).files[s].type.toLowerCase();
                            if (fileType.indexOf(allowedExtension[o]) > -1) {
                                extensionPass = false;
                            }
                        }

                        if (extensionPass) {
                            alert("Allowed image formats are jpg, jpeg and png");
                            return false;
                        }
                    }
                }
                var pictureInput = $(ev.currentTarget)[0];
                var myFormData = new FormData();
                for (var i in pictureInput.files) {
                    if (!isNaN(i)) {
                        myFormData.append('image'+i, pictureInput.files[i]);
                    }
                }

                function updateProgress(evt) {
                    // console.log('updateProgress');
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total * 100 + '%');
                        // console.log(percentComplete);
                        if (evt.loaded !== evt.total) {
                            $('.upload-progress').width(percentComplete);
                        } else {
                            $('.upload-progress').width(0);
                        }

                    } else {
                        // Unable to compute progress information since the total size is unknown
                        console.log('unable to complete');
                    }
                }

                $.ajax({
                    url: 'upload_image',
                    type: 'POST',
                    data: myFormData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    xhr: function() {  // custom xhr
                        myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // check if upload property exists
                            myXhr.upload.addEventListener('progress',updateProgress, false); // for handling the progress of the upload
                        }
                        return myXhr;
                    },
                    success: function(result) { console.log(result)
                        var imgInput = '<input type="hidden" name="image0" value="' + result.image0.path + '" class="recipe_images">';
                        var image = '<img src="http://api.helloself.co/' + result.image0.path + '">';
                        $("#hidden-image").html(imgInput);
                        $(".image").html(image);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }
        });

        var AccountDetailsBlock = Backbone.View.extend({
            el: '.account-menu',
            render: function() {
                var account = new AccountDetails();
                var that = this;
                account.fetch({
                    success: function (account) {
                        var template = _.template($("#account-details-block-template").html(), {account: account});
                        that.$el.html(template);
                    }
                });

            },
            events: {
                'click #accountOpen' : 'dropDown'
            },
            dropDown: function(ev) {
                $(ev.currentTarget).next().toggle();

                return false;
            }
        });

        /* END VIEWS */

        /* ROUTERS */

        var Router = Backbone.Router.extend({
            routes: {
                '' : 'home',
                'recipe-list' : 'home',
                'new-recipe' : 'edit-recipe',
                'edit-recipe/:id' : 'edit-recipe',
                'recipe-details/:id' : 'details',
                'logout' : 'logout',
                'profile' : 'profile',
                'account' : 'account',
                'searchbygroup/:group' : 'searchbygroup'
            }
        });

        var recipeList = new RecipeList();
        var editRecipe = new EditRecipe();
        var recipeDetails = new RecipeDetails();
        var logout = new Logout();
        var categoriesManu = new ManyCategoriesView();
        var comapnyProfileForm = new CompanyProfileForm();
        var companyDetails = new CompanyDetailsView();
        var accountForm = new AccountDetailsForm();
        var accountBlock = new AccountDetailsBlock();

        var router = new Router();

        router.on('route:home', function() {
            recipeList.render();
            categoriesManu.render();
            companyDetails.render();
            accountBlock.render();
        });

        router.on('route:edit-recipe', function(id) {
            editRecipe.render({id: id});
            categoriesManu.render();
            companyDetails.render();
            accountBlock.render();
        });

        router.on('route:details', function(id) {
            recipeDetails.render({id: id});
            categoriesManu.render();
            companyDetails.render();
            accountBlock.render();
        });

        router.on('route:logout', function(id) {
            logout.render({id: id});
        });

        router.on('route:profile', function() {
            comapnyProfileForm.render({id: 123});
            categoriesManu.render();
            companyDetails.render();
            accountBlock.render();
        });

        router.on('route:account', function() {
            accountForm.render({id: 123});
            categoriesManu.render();
            companyDetails.render();
            accountBlock.render();
        });

        router.on('route:searchbygroup', function(group) {
            recipeList.render({group: group});
            categoriesManu.render();
            companyDetails.render();
            accountBlock.render();
        });

        /* END ROUTERS */

        Backbone.history.start();
    </script>
</body>
</html>