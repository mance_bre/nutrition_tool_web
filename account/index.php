<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sign In</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/site-style.css">

    <style type="text/css">
        #overlay {
            background-color: rgba(0, 0, 0, 0.8);
            z-index: 999;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            display: none;
        }​
    </style>
</head>
<body class="site-page">

    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.2/underscore-min.js" type="text/javascript"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/0.9.2/backbone-min.js"></script>
    <script type="text/javascript" src="../js/vendor/jquery.select.js"></script>

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="back-image">
        <div id="header" class="header">
            <div class="container">
                <h1 class="logo"><a href="/">HelloSelf Owners</a></h1>
                <nav class="nav">
                    <a href="/#/features">Features</a>
                    <a href="">Pricing</a>
                    <a href="../account/#/signin" class="current">Sign In</a>
                    <a href="../account/#/signup" class="signup-button">Sign up for HelloSelf</a>
                </nav>
            </div>
        </div>

        <div id="main" class="container signup-container">
        </div>
    </div>

    <?php include '../footer.php';?>

    <script type="text/template" id="signin-template">
        <h2>Sign In</h2>
        <p class="intro-text"><a href="/blog"><%= "See what's new" %> »</a></p>
        <div class="form-holder signin-form">
            <form class="signin">
                <div class="form-error-message"></div>
                <input type="text" name="email" class="name-field" placeholder="Email Address" value="<%= window.localStorage.getItem('email') %>">
                <input type="password" name="pass" class="password-field" placeholder="Password" value="<%= window.localStorage.getItem('password') %>">
                <label for="remember-field">
                    <input type="checkbox" name="remember" id="remember-field" class="remember-field">
                    Remember me
                </label>
                <button name="submit" value="submit" type="submit" class="signin-button">Sign in to HelloSelf</button>
                <p class="other-message"><a href="../account/#/forgot-password" class="forgot-password">Forgot Password? »</a></p>
            </form>
        </div>
    </script>

    <script type="text/template" id="signup-template">
        <h2>New to HelloSelf? Sign Up</h2>
        <p class="intro-text">Create a free account and start managing your recipes</p>
        <div class="form-holder signup-form">
            <form class="signup">
                <div class="form-error-message"></div>
                <input type="text" name="user" class="name-field" placeholder="Your Name">
                <input type="text" name="email" class="email-field" placeholder="Email Address">
                <input type="password" name="pass" class="password-field" placeholder="Password">
                <input type="password" name="repass" class="repeat-password-field" placeholder="Repeat Password">

                <button name="submit" value="submit" type="submit" class="signin-button">Sign up for HelloSelf</button>
                <p class="other-message">Have an account? <a href="../account/#/signin">Sign in »</a></p>
            </form>
        </div>
    </script>

    <script type="text/template" id="forgot-password-template">
            <h2>Forgot your password?</h2>
            <p class="intro-text">Recover your HelloSelf password using the email address associated with your account.</p>
            <div class="form-holder forgot-password-form">
                <form class="forgot-password-form" action="">
                <div id="overlay"></div>
                    <input type="text" name="email" class="email-field" placeholder="Email Address">
                    <button class="signin-button">Reset your password</button>
                    <p class="other-message"><a href="../account/#/signin">Cancel »</a></p>
                </form>
            </div>
    </script>

    <script type="text/template" id="reset-password-template">
            <h2>Reset your password?</h2>
            <p class="intro-text">Enter and save your new password.</p>
            <div class="form-holder reset-password-form">
                <form class="reset-password-form" action="">
                <div id="overlay"></div>
                    <input type="password" name="pass" class="password-field" placeholder="New Password">
                    <input type="hidden" name="email" value="<%=window.localStorage.getItem("helloemail")%>">
                    <button class="signin-button">Save your new password</button>
                    <p class="other-message"><a href="/signin">Cancel »</a></p>
                </form>
            </div>
    </script>

    <script>

        var apiURL = "http://api.helloself.co:8080/";

        if (window.location.host === "localhost") {
            apiURL = "http://127.0.0.1:8080/";
        }

        $.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
            options.url = apiURL + options.url;
        });

        $.fn.serializeObject = function() {
          var o = {};
          var a = this.serializeArray();
          $.each(a, function() {
              if (o[this.name] !== undefined) {
                  if (!o[this.name].push) {
                      o[this.name] = [o[this.name]];
                  }
                  o[this.name].push(this.value || '');
              } else {
                  o[this.name] = this.value || '';
              }
          });
          return o;
        };

        function getSearchParameters() {
              var prmstr = window.location.search.substr(1);
              return prmstr !== null && prmstr !== "" ? transformToAssocArray(prmstr) : {};
        }

        function transformToAssocArray( prmstr ) {
            var params = {};
            var prmarr = prmstr.split("&");
            for ( var i = 0; i < prmarr.length; i++) {
                var tmparr = prmarr[i].split("=");
                params[tmparr[0]] = tmparr[1];
            }
            return params;
        }

        var UserLogin = Backbone.Model.extend({
            urlRoot: '/account/login'
        });

        var UserSignup = Backbone.Model.extend({
            urlRoot: '/account/signup'
        });

        var ForgotPassword = Backbone.Model.extend({
            urlRoot: '/account/lost-password'
        });

        var ResetPasswordLink = Backbone.Model.extend({
            urlRoot: '/account/reset-password'
        });

        var AccountHome = Backbone.View.extend({
            el: '#main',
            render: function() {
                var access_token = window.localStorage.getItem('access_token');
                var token_expires = window.localStorage.getItem('token_expires');
                var unixNow = (new Date).getTime();
                if (!access_token || !token_expires || unixNow > token_expires) {
                    router.navigate('/#/signin', {trigger:true});
                    // console.log(access_token, token_expires);
                    // console.log('token_expires', unixNow > token_expires);
                } else {
                    this.$el.html('<h2>Account details page is under construction, please check out our recipe <a href="/app">dashboard</a></h2>');
                }
                document.title = 'Account Home';
            }
        });

        var UserLoginPage = Backbone.View.extend({
            el: '#main',
            render: function() {
                var template = _.template($("#signin-template").html(), {});
                this.$el.html(template);
                document.title = 'Sign In';
            },
            events: {
                'submit .signin' : 'login'
            },
            login: function(ev) {
                if ($("#remember-field").is(':checked') && $(".name-field").val() !== '' && $(".password-field").val() !== '') {
                    window.localStorage.setItem("email", $(".name-field").val());
                    window.localStorage.setItem("password", $(".password-field").val());
                } else {
                    window.localStorage.removeItem('email');
                    window.localStorage.removeItem('password');
                }
                var userData = $(ev.currentTarget).serializeObject();
                var userLogin = new UserLogin();
                $(".form-error-message").hide();
                $(".signin input").removeClass("error-field");
                userLogin.save(userData, {
                    success: function(model, response) {
                        if (response.message) {
                            alert(response.message);
                        }
                        window.localStorage.setItem("user_id", response.user._id);
                        window.localStorage.setItem("user_email", response.user.email);
                        window.localStorage.setItem("user_role", response.user.role);
                        window.localStorage.setItem("access_token", response.token);
                        window.localStorage.setItem("token_expires", response.expires);

                        router.navigate('/#/account', {trigger:true});
                    },
                    error: function(model, response) {
                        if (response.responseText) {
                            $(".form-error-message").show().text(response.responseText);
                            if (response.responseText.indexOf('Email') > -1) {
                                $('input[name=email]').addClass("error-field");
                            } else if (response.responseText.indexOf('password') > -1) {
                                $('input[name=pass]').addClass("error-field");
                            }
                        }
                    }
                });

                return false;
            }
        });

        var UserSignupPage = Backbone.View.extend({
            el: '#main',
            render: function() {
                var template = _.template($("#signup-template").html(), {});
                this.$el.html(template);
                document.title = 'Sign Up';
            },
            events: {
                'submit .signup' : 'signup'
            },
            signup: function(ev) {
                var userData = $(ev.currentTarget).serializeObject();
                var userSignup = new UserSignup();
                $(".form-error-message").hide();
                $(".signup input").removeClass("error-field");
                userSignup.save(userData, {
                    success: function(model, response) {
                        if (response.message) {
                            alert(response.message);
                        }

                        router.navigate('/#/signin', {trigger:true});
                    },
                    error: function(model, response) {
                        if (response.responseText) {
                            $(".form-error-message").show().text(response.responseText);
                            if (response.responseText.indexOf('Email') > -1 || response.responseText.indexOf('email') > -1) {
                                $('input[name=email]').addClass("error-field");
                            }
                            if (response.responseText.indexOf('Password') > -1 || response.responseText.indexOf('password') > -1) {
                                $('input[name=pass]').addClass("error-field");
                            }
                            if (response.responseText.indexOf('Retype') > -1) {
                                $('input[name=repass]').addClass("error-field");
                            }
                            if (response.responseText.indexOf('User') > -1 || response.responseText.indexOf('username') > -1) {
                                $('input[name=user]').addClass("error-field");
                            }
                        }
                    }
                });

                return false;
            }
        });

        var ForgotPasswordPage = Backbone.View.extend({
            el: '#main',
            render: function() {
                var template = _.template($("#forgot-password-template").html(), {});
                this.$el.html(template);
                document.title = 'Forgot Password';
            },
            events: {
                'submit .forgot-password-form' : 'sendForgotPassEmail'
            },
            sendForgotPassEmail: function(ev) {
                $("#overlay").show();
                var emailData = $(ev.currentTarget).serializeObject();
                var forgotPassword = new ForgotPassword();
                forgotPassword.save(emailData, {
                    dataType:"text",
                    success: function(model, response) {
                        $("#overlay").hide();
                        alert("Password reset link is sent to your email. Please check your email");
                        router.navigate('/#/signin', {trigger:true});
                    },
                    error: function(model, response) {
                        alert(response.responseText);
                        $("#overlay").hide();
                    }
                });

                return false;
            }
        });

        var ResetPasswordLinkPage = Backbone.View.extend({
            el: '#main',
            render: function() {
                var resretPasswordLink = new ResetPasswordLink();
                var params = getSearchParameters();
                resretPasswordLink.fetch({
                    data: { e: decodeURIComponent(params.e), p: params.p.replace('/', '')},
                    dataType:"text",
                    success: function(model, response) {
                        window.localStorage.setItem("helloemail", params.e);
                        // router.navigate('/#/new-password', {trigger:true});
                        window.location.replace("../account/#/new-password");
                    },
                    error: function(model, response) {
                        alert(response.responseText);
                        router.navigate('/#/forgot-password', {trigger:true});
                    }
                });
            }
        });

        var NewPasswordPage = Backbone.View.extend({
            el: '#main',
            render: function() {
                var template = _.template($("#reset-password-template").html(), {});
                this.$el.html(template);
                document.title = 'Forgot Password';
            },
            events: {
                'submit .reset-password-form' : 'sendPassword'
            },
            sendPassword: function(ev) {
                var data = $(ev.currentTarget).serializeObject();
                var newPassword = new ResetPasswordLink();
                newPassword.save(data, {
                    dataType:"text",
                    success: function(model, response) {
                        alert("You have successfully saved your new password");
                        router.navigate('/#/signin', {trigger:true});
                    },
                    error: function(model, response) {
                        alert(response.responseText);
                    }
                });

                return false;
            }
        });

        var Router = Backbone.Router.extend({
            routes: {
                'account': 'home',
                'signin': 'signin',
                'signup': 'signup',
                'forgot-password': 'forgot',
                'reset-password': 'resetLink',
                'new-password': 'newPassword'
            }
        });

        var userLoginPage = new UserLoginPage();
        var accountHome = new AccountHome();
        var userSignupPage = new UserSignupPage();
        var forgotPasswordPage = new ForgotPasswordPage();
        var resetPasswordLinkPage = new ResetPasswordLinkPage();
        var newPasswordPage = new NewPasswordPage();

        var router = new Router();

        router.on('route:home', function() {
            accountHome.render();
        });

        router.on('route:signin', function() {
            userLoginPage.render();
        });

        router.on('route:signup', function() {
            userSignupPage.render();
        });

        router.on('route:forgot', function() {
            forgotPasswordPage.render();
        });

        router.on('route:resetLink', function() {
            resetPasswordLinkPage.render();
        });

        router.on('route:newPassword', function() {
            newPasswordPage.render();
        });

        Backbone.history.start();
    </script>
</body>
</html>