<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Menu with HelloSelf</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/site-style.css">
</head>
<body class="site-page">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="back-image">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
        <?php include 'header.php';?>

        <div class="features-holder">
            <h2>Simple, useful features to help you better manage your restaurant menu online.</h2>
            <div class="features-image">
                <div class="pulse-one-holder">
                    <button class='pulse-button pulse-one'>&plus;</button>
                    <div class="pulse-content pulse-one-content">
                        <h4>Automatic Dish Categorization</h4>
                        <p>Instantly receive a food plan label based on the ingredients mentioned in each dish. Labeled dishes help you easily meet specific dietary requests and improve your menu recommendations to customers.</p>
                    </div>
                </div>
                <div class="pulse-two-holder">
                    <button class='pulse-button pulse-two'>&plus;</button>
                    <div class="pulse-content pulse-two-content">
                        <h4>Certified Nutrition Labeling</h4>
                        <p>Import your recipes for a nutritionist certified dish analysis. By giving your customers this information now, you will have a competitive advantage over other restaurants that will soon be required by law to do so.</p>
                    </div>
                </div>
                <div class="pulse-three-holder">
                    <button class='pulse-button pulse-three'>&plus;</button>
                    <div class="pulse-content pulse-three-content">
                        <h4>Expanded Dish Details</h4>
                        <p>Quickly add recipe instructions, modifications or even add comments to a dish. By keeping an organized directory, you’ll be able to spend more time in the kitchen perfecting your creations.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="main" class="container">

        <hr class="separator">

        <div class="featured-section left-image">
            <div class="img-holder">
                <img src="../img/upload-menu.png" title="Upload Your Menu in Minutes" alt="Upload Your Menu in Minutes" width="241px">
            </div>
            <div class="featured-item">
                <h2>Upload Your Menu in Minutes</h2>
                <p>Whether you store your recipes in a Dropbox folder or a Google Doc. We got you covered by supporting various file formats.</p>
            </div>
        </div>

        <hr class="separator">

        <div class="featured-section right-image">
            <div class="img-holder">
                <img src="../img/usda-nutrition.png" title="USDA Recognized Nutrition Database" alt="USDA Recognized Nutrition Database" width="297px">
            </div>
            <div class="featured-item">
                <h2>USDA Recognized Nutrition Database</h2>
                <p>You're getting the correct results with our nutritionist backed ingredient calculations. Based off USDA standards and with 100% accuracy you can trust.</p>
            </div>
        </div>

        <hr class="separator">

        <div class="featured-section left-image">
            <div class="img-holder">
                <img src="../img/diet-type.png" title="Identify Dishes By Diet Type" alt="Identify Dishes By Diet Type" width="275px">
            </div>
            <div class="featured-item">
                <h2>Identify Dishes By Diet Type</h2>
                <p>From Vegetarian, Gluten-free to the Paleo diet, we'll identify your meals by one of our 13 diet types. Labels help your servers communicate better and give your customers a bigger selection.</p>
            </div>
        </div>

        <hr class="separator">

        <div class="featured-section right-image">
            <div class="img-holder">
                <img src="../img/badge-manage@2x.png" title="Effortlessly Manage Your Recipes" alt="Effortlessly Manage Your Recipes" width="280px">
            </div>
            <div class="featured-item">
                <h2>Effortlessly Manage Your Recipes</h2>
                <p>Our menu management dashboard lets you edit, share and comment on all your recipes. Cheers to staying organized and secure.</p>
            </div>
        </div>

        <hr class="separator">

        <div class="featured-section featured-last-list">
            <h2>And More…</h2>
            <ul class="featured-list">
                <li>
                    <h3>Increase Visibility With HelloSelf Support Materials</h3>
                    <p>Get your HelloSelf marketing materials as part of your plan. You'll be a proud supporter of real food with our storefront window decal and website badges.</p>
                </li>
                <li>
                    <h3>Nutrition Implementation Coaching</h3>
                    <p>Get access to our bonuses and private onboarding webinars led by our in-house nutritionist, Megan Wolf. She will teach you how to increase revenue with healthy ingredient modifications.</p>
                </li>
                <li>
                    <h3>Share Your Food Philosophy</h3>
                    <p>Use our platform as a marketing tool to tell the world the story behind your food. Take pride in the high quality, sustainable and locally sourced ingredients you serve.</p>
                </li>
            </ul>
        </div>

    </div>
    <div class="call-to-action">
        <div class="container">
            <p class="action-copy">Start analyzing and managing your recipes and get certified nutrition facts today.</p>
            <a href="./account/#/signup" class="free-account-button">Get Your Free Account</a>
        </div>
    </div>

    <?php include 'footer.php';?>

</body>
</html>