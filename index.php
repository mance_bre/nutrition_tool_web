<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Menu with HelloSelf</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/site-style.css">
</head>
<body class="site-page site-home-page">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="back-image">
        <?php include 'header.php';?>
    </div>

    <div class="product-section">
        <div class="container">
            <div class="headline">
                <p>Easily upload your menu</p>
                <h2>Help your customers discover food they desire</h2>
            </div>
            <ul class="product-list">
                <li>
                    <img src="../img/badge-manage@2x.png" height="115px">
                    <h3>MANAGE YOUR RECIPES</h3>
                    <p>Manage your sustainably produced food within the <span>Menu Dashboard</span>. Effortlessly <span>add</span>, <span>edit</span> and <span>comment</span> on the results of your handcrafted dish.</p>
                </li>
                <li>
                    <img src="../img/badge-certified@2x.png" height="115px">
                    <h3>SCREEN YOUR MENU</h3>
                    <p>Use our <span>nutritional analysis tool</span> and instantly get a <span>nutritionist-certified</span> recipe screening. Our database follows <span>USDA</span> standards and your dishes will be verified upon completion.</p>
                </li>
                <li>
                    <img src="../img/badge-labeling@2x.png" height="115px">
                    <h3>LABELING AND CATEGORIZATION</h3>
                    <p>Let us <span>sort your dishes</span> into the most common food plan trends your customers may be on. Whether that's <span>Gluten-free</span> or even <span>Paleo</span>, we'll help make them visible.</p>
                </li>
            </ul>
            <a href="/features.php" class="see-features-button">See All Features</a>
        </div>
    </div>

    <div class="testimonials-section">
        <div class="container">
            <div class="headline">
                <p>Testimonials</p>
                <h2>What Our Customers Say</h2>
            </div>
            <ul class="testimonials-list">
                <li>
                    <img src="../img/test-account-image.png">
                    <p>It’s fast, it’s functional, easy to use, clear, beautyfull and help me to have more chooses to my customers <strong>- Fernando Coppola - Sport Bar and Snacks</strong></p>
                </li>
                <li>
                    <img src="../img/test-account-image.png">
                    <p>It’s fast, it’s functional, easy to use, clear, beautyfull and help me to have more chooses to my customers <strong>- Fernando Coppola - Sport Bar and Snacks</strong></p>
                </li>
            </ul>
        </div>
    </div>

    <div class="you-know-section">
        <div class="container">
            <h4>DID YOU KNOW?</h4>
            <p>There is greater public awareness and pressure on the restaurant industry to produce more healthful offerings.</p>
            <p>In fact, almost 75% of all adults are more likely to visit a restaurant that offers healthy options</p>
            <a href="./account/#/signup" class="free-account-button">Get Your Free Account</a>
        </div>
    </div>

    <div class="screenshoots-section">
        <div class="container">
            <div class="headline">
                <p>Simple and user-friendly dashboard</p>
                <h2>Showcasing your offerings in a beautiful, plated presentation</h2>
            </div>
            <div class="app-img">
            </div>
        </div>
    </div>

    <?php include 'footer.php';?>

</body>
</html>