<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sign In</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/site-style.css">
</head>
<body class="site-page">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="back-image">

        <?php include 'header.php';?>

        <div id="main" class="container signin-container">
            <h2>Sign In</h2>
            <p class="intro-text"><a href="/blog">See what's new »</a></p>
            <div class="form-holder signin-form">
                <form action="">
                    <input type="text" name="" class="email-field" placeholder="Email Address">
                    <input type="password" name="" class="password-field" placeholder="Password">
                    <label for="remember-field">
                        <input type="checkbox" name="" id="remember-field" class="remember-field">
                        Remember me
                    </label>
                    <button class="signin-button">Sign in to HelloSelf</button>
                    <p class="other-message"><a href="/forgot-password" class="forgot-password">Forgot Password? »</a></p>
                </form>
            </div>
        </div>
    </div>

    <?php include 'footer.php';?>

</body>
</html>