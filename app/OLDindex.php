<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body class="dashboard-page">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="wrapper">

        <div id="header" class="header">

            <ul class="nav" role="navigation">
                <li><a href="" class="analyze-recipe-button">Analyze Recipes</a></li>
                <li><a href="/app/new-recipe.php">New Recipe</a></li>
                <li><a href="">Export</a></li>
            </ul>

            <a href="javasctipt:void(0);" class="account-menu">
                <img src="../img/test-account-image.png" width="40px">
                <span class="account-data">
                    <span class="account-name">Jovan Stojanovic</span>
                    <span class="account-role">Manager at Agra Culture</span>
                </span>
                <span class="arrow-bottom"></span>
            </a>

            <p class="upgrade-message">A La Carte - <a href="">Upgrade to Full Course</a></p>

        </div>

        <div id="main" class="container">

            <div class="sidebar">

                <div class="brand">
                    <a href="/app/">
                        <img src="../img/test-logo.png" width="118px">
                    </a>
                    <h2>Agra Culture</h2>
                    <a href="http://www.agra-culture.com">www.agra-culture.com</a>
                </div>

                <ul class="sidebar-nav">
                    <li class="title">MENU</li>
                    <li><a href="" class="current">All Items →</a></li>
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU CATEGORIES</li>
                    <li><a href="">- Breakfast</a></li>
                    <li><a href="">- Lunch/Dinner</a></li>
                    <li><a href="">- Drinks</a></li>
                    <li><a href="">- Kids</a></li>
                    <li><a href="">- Dessert</a></li>
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU LABELING</li>
                    <li><a href="">- Paleo</a></li>
                    <li><a href="">- Vegetarian</a></li>
                    <li><a href="">- Vegan</a></li>
                    <li><a href="">- Heart Healthy</a></li>
                    <li><a href="">- Dairy Free</a></li>
                    <li><a href="">- Gluten Free</a></li>
                    <li><a href="">- High Protein</a></li>
                    <li><a href="">- Low Carb</a></li>
                    <li><a href="">- Low Sodium</a></li>
                    <li><a href="">- Low Calorie</a></li>
                    <li><a href="">- Low Fat</a></li>
                    <li><a href="">- Mediterranean</a></li>
                    <li><a href="">- Pescetarian</a></li>
                </ul>

            </div>

            <div class="content">
                <div class="headline">
                    <h2>All Items</h2>
                    <form action="" class="search-form">
                        <input name="search" type="search" placeholder="Search" class="search-field">

                        <div class="search-results">
                            <ul>
                                <li class="nothing-found">
                                    <p>Sorry, nothing found!</p>
                                    <span>Accross your dishes.</span>
                                </li>
                                <li>
                                    <p>Seared Angus Beef</p>
                                    <span>in Lunch/Dinner</span>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>

                <div class="actions-holder">
                    <a href="javascript:void(0);" class="single-filter">Select All</a>

                    <a href="javascript:void(0);" class="double-filter current">Newest</a>
                    <a href="javascript:void(0);" class="double-filter last">Oldest</a>

                    <a href="javascript:void(0);" class="double-filter">Verified</a>
                    <a href="javascript:void(0);" class="double-filter last">Unverified</a>

                    <div class="sort">
                        <span>Sort:</span>
                        <a href="javascript:void(0);" class="double-filter current">A-Z</a>
                        <a href="javascript:void(0);" class="double-filter more-filters">&nbsp;</a>
                    </div>
                </div>

                <div class="results-holder">
                    <ul class="results">
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                            <div class="result-info">
                                <input type="checkbox" name="select" class="checkbox">
                                <h3>Kale Caesar</h3>
                                <p>chorizo, organic peppers, black beans, pepper jack, cilantro…</p>
                                <div class="category-info">
                                    <span class="cate">Lunch/Dinner</span>
                                    <span class="nutrilabel">Vegetarian, Vegan</span>
                                </div>
                                <div class="nutrition-info">
                                    <span>CAL: 510</span>
                                    <span>FAT: 39g</span>
                                    <span>CARBS: 8g</span>
                                </div>
                                <a href="/app/details.php" class="details-button">View Details</a>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                        <li class="results-item">
                            <a href="">
                            <div class="result-image">
                                <span class="verified">Verified</span>
                            </div>
                            </a>
                            <div class="result-details">
                                <h3><a href="">Kale Caesar</a></h3>
                                <span class="recipe-category">in Lunch/Dinner</span>
                                <span class="recipe-time">21d Ago</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="pagination-holder">
                    <a href="" class="last-pag">Prev</a>
                    <a href="">Next</a>
                </div>
            </div>

        </div>

        <div id="footer" class="footer">

        </div>
    </div>
    <script src="../js/vendor/jquery-2.1.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function(){

        });
    </script>
</body>
</html>