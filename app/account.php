<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body class="new-recipe-page">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="wrapper">

        <div id="header" class="header">

            <ul class="nav" role="navigation">
                <li><a href="" class="analyze-recipe-button">Analyze Recipes</a></li>
                <li><a href="/app/new-recipe.php">New Recipe</a></li>
                <li><a href="">Export</a></li>
            </ul>

            <a href="javasctipt:void(0);" class="account-menu">
                <img src="../img/test-account-image.png" width="40px">
                <span class="account-data">
                    <span class="account-name">Jovan Stojanovic</span>
                    <span class="account-role">Manager at Agra Culture</span>
                </span>
                <span class="arrow-bottom"></span>
            </a>

            <p class="upgrade-message">A La Carte - <a href="">Upgrade to Full Course</a></p>

        </div>

        <div id="main" class="container">

            <div class="sidebar">

                <div class="brand">
                    <a href="/app/">
                        <img src="../img/test-logo.png" width="118px">
                    </a>
                    <h2>Agra Culture</h2>
                    <a href="http://www.agra-culture.com">www.agra-culture.com</a>
                </div>

                <ul class="sidebar-nav">
                    <li class="title">MENU</li>
                    <li><a href="" class="current">All Items →</a></li>
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU CATEGORIES</li>
                    <li><a href="">- Breakfast</a></li>
                    <li><a href="">- Lunch/Dinner</a></li>
                    <li><a href="">- Drinks</a></li>
                    <li><a href="">- Kids</a></li>
                    <li><a href="">- Dessert</a></li>
                </ul>

                <ul class="sidebar-nav">
                    <li class="title">MENU LABELING</li>
                    <li><a href="">- Paleo</a></li>
                    <li><a href="">- Vegetarian</a></li>
                    <li><a href="">- Vegan</a></li>
                    <li><a href="">- Heart Healthy</a></li>
                    <li><a href="">- Dairy Free</a></li>
                    <li><a href="">- Gluten Free</a></li>
                    <li><a href="">- High Protein</a></li>
                    <li><a href="">- Low Carb</a></li>
                    <li><a href="">- Low Sodium</a></li>
                    <li><a href="">- Low Calorie</a></li>
                    <li><a href="">- Low Fat</a></li>
                    <li><a href="">- Mediterranean</a></li>
                    <li><a href="">- Pescetarian</a></li>
                </ul>

            </div>

            <div class="content">
                <div class="headline">
                    <h2>Account Settings</h2>
                </div>

                <div class="form-holder">
                    <form action="">

                        <fieldset>
                            <label for="user-name">User Name</label>
                            <input id="user-name" type="text" name="" placeholder="User Name" class="input-field">
                        </fieldset>
                        <fieldset>
                            <label for="first-last-name">First/Last Name</label>
                            <input id="first-last-name" type="text" name="" placeholder="First Last Name" class="input-field">
                        </fieldset>
                        <fieldset>
                            <label for="email-address">Email Address</label>
                            <input id="email-address" type="text" name="" placeholder="Email Address" class="input-field">
                        </fieldset>
                        <fieldset>
                            <label for="company-role">Company Role</label>
                            <input id="company-role" type="text" name="" placeholder="eg. Lead Chef" class="input-field">
                        </fieldset>
                        <fieldset>
                            <label for="phone-number">Phone Number</label>
                            <input id="phone-number" type="text" name="" placeholder="eg. +(298) 223-234-22" class="input-field">
                        </fieldset>
                        <fieldset>
                            <button class="new-recipe-button">Submit</button>
                        </fieldset>
                    </form>
                    <div class="floating-right">
                        <form action="">
                            <fieldset>
                                <label for="upload-images">Image</label>
                                <div class="upload-single-image user-image">
                                    <a href="javascript:void(0);" class="upload-image-button">
                                        Upload
                                        <input type="file" name="" class="upload-field">
                                    </a>
                                    <div class="upload-progress"></div>
                                </div>
                            </fieldset>
                            <div class="upgrade-plan">
                                <p>PRICING PLAN</p>
                                <h2>A La Carte</h2>
                                <a href="/">Upgrade to Full Course</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

        <div id="footer" class="footer">

        </div>
    </div>
    <script src="../js/vendor/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="../js/vendor/jquery.select.js"></script>
    <script type="text/javascript">
        $(document).ready( function(){
            $('.select-field').selectOverlap();
        });
    </script>
</body>
</html>