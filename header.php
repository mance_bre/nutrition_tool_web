<div id="header" class="header">
    <div class="container">
        <h1 class="logo"><a href="/">HelloSelf Owners</a></h1>
        <nav class="nav">
            <a href="/" class="current">Home</a>
            <a href="/features.php">Features</a>
            <a href="/pricing.php">Pricing</a>
            <a href="./account/#/signin">Sign In</a>
            <a href="./account/#/signup" class="signup-button">Sign up for HelloSelf</a>
        </nav>
    </div>

    <div class="intro">
        <h2>Reach more health conscious customers with a nutritionist certified menu</h2>
        <div class="intro-content">
            <a href="./account/#/signup" class="create-account-button">Create a Free Account</a>
            <a href="/pricing.php" class="pricing-plans-button">See Pricing Details</a>
            <p>Restaurants sell more meals, customers are happier with more choices</p>
        </div>
        <span class="scroll-down-info">SCROLL DOWN TO SEE ALL BENEFITS</span>
    </div>
</div>