# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* HelloSelf Converting Tool
* 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Important ###
* Keep Master as dev repo
* Before we push some changes to QA server create branch with proper version example: "0.1"


### How do I get set up? ###

* Install Bundler
* Install Sass
* Install Compass
* Install Susy
* Go to "HelloSelf-Web" project and run "bundler install" then hit "bundler exec compass watch" to compile css file
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact