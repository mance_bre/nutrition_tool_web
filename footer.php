<div id="footer" class="footer">
    <div class="container">
        <div class="footer-section">
            <h1 class="logo"><a href="/">HelloSelf Owners</a></h1>
            <p>© HelloSelf 2014</p>
        </div>
        <div class="footer-section">
            <h3>PRODUCT</h3>
            <a href="/features.php">Features</a>
            <a href="/pricing.php">Pricing</a>
            <a href="">Analyze Recipe</a>
        </div>
        <div class="footer-section">
            <h3>COMPANY</h3>
            <a href="/about.php">About Us</a>
            <a href="/contact.php">Contact Us</a>
        </div>
        <div class="footer-section">
            <h3>OTHER</h3>
            <a href="/terms/php">Terms of Use</a>
            <a href="/privacy.php">Privacy Policy</a>
        </div>
        <div class="footer-section social-section">
            <h3>GET OUR NEWSLETTER</h3>
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="//helloself.us3.list-manage.com/subscribe/post?u=cce12e3b27bb986177b1b23b6&amp;id=a1d4d5df41" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <label for="mce-EMAIL">Tips To Satisfy Health-Focused Diners</label>
                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Email Address" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;">
                            <input type="text" name="b_cce12e3b27bb986177b1b23b6_a1d4d5df41" tabindex="-1" value="">
                        </div>
                        <input type="submit" value="Send" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </div>
                </form>
            </div>
            <!--End mc_embed_signup-->
            <div class="social-holder">
                <a href="https://www.facebook.com/HelloSelfApp" class="facebook-like-button">Like Us</a>
                <a href="https://twitter.com/helloself" class="twitter-follow-button">Follow Us</a>
            </div>
        </div>
        <p class="footer-message">Our nutrition database is based on the <a href="http://www.usda.gov/" target="_blank" rel="nofollow">USDA</a> industry best standards and led by our in-house registered dietician</p>
    </div>
</div>
<script type="text/javascript">
    /* Quick hack */
    if (!window.jQuery) {
        var jq = document.createElement('script'); jq.type = 'text/javascript';
        // Path to jquery.js file, eg. Google hosted version
        jq.src = '/js/vendor/jquery-2.1.1.min.js';
        document.getElementsByTagName('head')[0].appendChild(jq);
    }
    $(document).ready( function(){

        $('.pulse-one').click(function(){
            $('.pulse-one-content').toggle();
        });
        $('.pulse-two').click(function(){
            $('.pulse-two-content').toggle();
        });
        $('.pulse-three').click(function(){
            $('.pulse-three-content').toggle();
        });
    });
</script>
